<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;

class EmailController extends Controller
{
    public function sendEmail(Request $request)
    {
        $emailCustomer  = 'alfianriyan24@gmail.com';
        Mail::to($emailCustomer)->send(new TestEmail());
    }
}