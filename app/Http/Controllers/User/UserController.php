<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class UserController extends Controller
{
	public function userList(Request $request) {
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->get(config('aplikasi.apiUrl').'userID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);
		$data = json_decode($result->getBody());
		return view('user.userList', compact('data'));
	}	
}