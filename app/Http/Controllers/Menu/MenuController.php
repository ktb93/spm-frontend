<?php

namespace App\Http\Controllers\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
Use Cookie;

class MenuController extends Controller
{
    public static function menuList()
    {
        $client = new Client();
		$result = $client->get(config('aplikasi.apiUrl').'menu', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]); 
		$obj = json_decode($result->getBody());
		$data = $obj->message;
		$levels=1;
		// dd($data);

        foreach ($data as $data2)
        {
        	$current_level=$data2->Level;

        	if( $data2->Level==1){
        		$text="fa arrow";
        	}else{
        		$text="";
        	}

        	if($current_level < $levels){
        		$difference=$levels-$current_level;
        		for ($i = 1; $i <= $difference; $i++){	
        			echo "</ul></li>";
        		}
        	}

        	if(strpos($data2->Link, '#') !== false)
			{
				//$link = $data2->LINK;
				$pisah = explode('#',$data2->Link);
				$link = $pisah[1];
			}
			else
			{
				$link = $data2->Link;

			}
            if ($data2->Submenu == "1") {
            	echo "<li class='nav xn-openable'> <a href='".url($link)."'><span class='fa  fa-".$data2->Icon." fa-fw'></span> " . $data2->Name . "<span class='".$text."'></span></a></li>";
            }else{
				echo "<li class='xn-openable'><a href='#'><span class='fa  fa-".$data2->Icon." fa-fw'></span> " . $data2->Name . " <span class='".$text."'> </span></a><ul class='nav nav-second-level'>";
            }

            $levels=$current_level;				
		}

        if($levels = 1){
        	echo "</ul></li>";
        }
    }
}
