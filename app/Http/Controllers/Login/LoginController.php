<?php

namespace App\Http\Controllers\Login;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class LoginController extends Controller
{
	public function indexLogin(Request $request) {
		return view('auth.login');
	}	

	public function loginApp(Request $request)
	{
        $username = $request->username;
		$password = $request->password;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'login', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'username' => $username,
				'password' => $password
			]
		]); 

		$obj = json_decode($result->getBody());
		$success = $obj->success;
		$msg = $obj->message;

		if($success == "true" || $success == "1")
		{	
			$redis = Redis::connection();
			Cache::put(config('aplikasi.codeRedis').$obj->api_token, $obj->message, config('aplikasi.cookieExpired'));
			Cookie::queue('api_token', $obj->api_token, config('aplikasi.cookieExpired'));
			Cookie::queue('password_encrypt', $obj->password_encrypt, config('aplikasi.cookieExpired'));
			return response()->json(array('success'=> $success, 'msg'=> $msg), 200);			
		}
		else
		{
			return response()->json(array('success'=> $success, 'msg'=> $msg), 200);
		}
	}

	public function logoutApp(Request $request)
	{
		$token = Cookie::get('api_token');
		// var_dump($token);
		// die();
		Cache::forget(config('aplikasi.codeRedis').$token);
		Cookie::forget('api_token');
		return redirect('login'); 
	}
}