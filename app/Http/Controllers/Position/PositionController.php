<?php

namespace App\Http\Controllers\Position;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class PositionController extends Controller
{
	public function positionList(Request $request) {
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->get(config('aplikasi.apiUrl').'positionID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);
		$data = json_decode($result->getBody());
		return view('position.positionList', compact('data'));
	}	
}