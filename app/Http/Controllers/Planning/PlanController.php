<?php

namespace App\Http\Controllers\Planning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class PlanController extends Controller
{
	public function planExpense(Request $request) {
		return view('planning.plan');
	}

	public function detailPlanExpense(Request $request) {
		$kd_kontrak = $request->kd_kontrak;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'detailContract', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'kd_kontrak' => $kd_kontrak
			]
		]);

		$result2 = $client->get(config('aplikasi.apiUrl').'planID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);

		$data = json_decode($result->getBody());
		$data = $data->data[0];
		$nilai_kontrak = number_format($data->nilai_kontrak,0,",",",");
		$planID = json_decode($result2->getBody());

		return view('planning.planDetail', compact('data', 'planID', 'nilai_kontrak'));
	}
	
	public function detailAccount(Request $request) {
		return view('planning.detailAccount');
	}

	public function viewPlanExpense(Request $request) {
		$kd_kontrak = $request->kd_kontrak;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'detailContract', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'kd_kontrak' => $kd_kontrak
			]
		]);

		$result2 = $client->post(config('aplikasi.apiUrl').'getDetailPlan', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'contractID' => $kd_kontrak
			]
		]);

		$data = json_decode($result->getBody());
		$data = $data->data[0];
		$nilai_kontrak = number_format($data->nilai_kontrak,0,",",",");
		$planData = json_decode($result2->getBody());
		$plan = '';
		if ($planData->success == true){
			$plan = $planData->data[0];
		}

		return view('planning.viewPlan', compact('data', 'plan', 'nilai_kontrak'));
	}
}