<?php

namespace App\Http\Controllers\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class ProjectController extends Controller
{
	public function projectData(Request $request) {
		// $test = Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"));
		// dd($test->id_user);
		return view('project.project');
	}

	public function newProject(Request $request) {
		$client = new Client(); //GuzzleHttp\Client
		
		$result = $client->get(config('aplikasi.apiUrl').'projectID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);

		$result2 = $client->get(config('aplikasi.apiUrl').'contractID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);
		$data['projectID'] = json_decode($result->getBody());
		$data['contractID'] = json_decode($result2->getBody());
		return view('project.newProject', compact('data'));
	}

	public function viewProject(Request $request) {
		$kd_proyek = $request->kd_proyek;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'detailProject', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'kd_proyek' => $kd_proyek
			]
		]);

		$result2 = $client->get(config('aplikasi.apiUrl').'contractID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);

		$contractID = json_decode($result2->getBody());
		$data = json_decode($result->getBody());
		$data = $data->data[0];
		$date_start = date("d-m-Y", strtotime($data->plan_start));
		$date_finish = date("d-m-Y", strtotime($data->plan_finish));
		return view('project.viewProject', compact('data','date_start','date_finish', 'contractID'));
	}	
}