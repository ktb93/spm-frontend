<?php

namespace App\Http\Controllers\AccountType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class AccTypeController extends Controller
{
	public function accTypeList(Request $request) {
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->get(config('aplikasi.apiUrl').'accTypeID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);
		$data = json_decode($result->getBody());
		return view('accountType.accTypeList', compact('data'));
	}	
}