<?php

namespace App\Http\Controllers\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class CustomerController extends Controller
{
	public function customerList(Request $request) {
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->get(config('aplikasi.apiUrl').'custID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);
		$data = json_decode($result->getBody());
		return view('customer.customerList', compact('data'));
	}	
}