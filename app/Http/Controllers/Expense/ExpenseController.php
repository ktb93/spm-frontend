<?php

namespace App\Http\Controllers\Expense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
Use Cookie;

class ExpenseController extends Controller
{
	public function expense(Request $request) {
		return view('expense.expense');
	}

	public function detailExpense(Request $request) {
		$kd_kontrak = $request->kd_kontrak;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'detailContract', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'kd_kontrak' => $kd_kontrak
			]
		]);

		$result2 = $client->get(config('aplikasi.apiUrl').'expenseID', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			]
		]);

		$data = json_decode($result->getBody());
		$data = $data->data[0];
		$nilai_kontrak = number_format($data->nilai_kontrak,0,",",",");
		$expenseID = json_decode($result2->getBody());

		return view('expense.expenseDetail', compact('data', 'expenseID', 'nilai_kontrak'));
	}
	
	public function detailAccount(Request $request) {
		return view('expense.detailAccount');
	}

	public function viewExpense(Request $request) {
		$kd_kontrak = $request->kd_kontrak;
		$client = new Client(); //GuzzleHttp\Client
		$result = $client->post(config('aplikasi.apiUrl').'detailContract', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'kd_kontrak' => $kd_kontrak
			]
		]);

		$result2 = $client->post(config('aplikasi.apiUrl').'getDetailExpense', [
			'headers' =>[
				'Authorization' => 'Basic Yml6bmV0OmJpem5ldA=='
			],
			'form_params' => [
				'contractID' => $kd_kontrak
			]
		]);

		$data = json_decode($result->getBody());
		$data = $data->data[0];
		$nilai_kontrak = number_format($data->nilai_kontrak,0,",",",");
		$expenseData = json_decode($result2->getBody());
		$expense = '';
		if ($expenseData->success == true){
			$expense = $expenseData->data[0];
		}

		return view('expense.viewExpense', compact('data', 'expense', 'nilai_kontrak'));
	}
}