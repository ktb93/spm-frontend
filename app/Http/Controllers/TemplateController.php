<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;

class TemplateController extends Controller
{
    public function dashboard()
    {
        return view('layouts.dashboard');
    }
    public function charts()
    {
        return view('mcharts');
    }
    public function tables()
    {
        return view('table');
    }
    public function forms()
    {
        return view('form');
    }
    public function panels()
    {
        return view('panel');
    }
    public function buttons()
    {
        return view('buttons');
    }
    public function notifications()
    {
        return view('notifications');
    }
    public function typography()
    {
        return view('typography');
    }
    public function icons()
    {
        return view('icons');
    }
    public function grid()
    {
        return view('grid');
    }
    public function blanks()
    {
        return view('blank');
    }
    public function login()
    {
        return view('login');
    }
    public function documentation()
    {
        return view('documentation');
    }
    public function progress()
    {
        return view('progressbars');
    }
}