<?php

namespace App\Http\Middleware;

use Closure;
Use Cookie;
use Illuminate\Support\Facades\Cache;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->path();
        $cache = Cache::get(config('aplikasi.codeRedis').Cookie::get('api_token'));   
        if ($cache == null && $url != 'login' && $url != '/') {
            return redirect('login');
        }elseif($cache != null && $url == 'login' || $url == '/'){
            return redirect('dashboard');
        }
        return $next($request);
    }
}
