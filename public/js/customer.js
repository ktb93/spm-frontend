$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formCustomerAdd').validate({
        rules: {
            email: {
                email: true
            }
        }         
    });
    $('#formCustomerEdit').validate({
        rules: {
            email: {
                email: true
            }
        }         
    });
});

function getCustomer(url,urlLoading,urlDelete){
    var table =  $('#customerTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ { "targets"  : 6, "orderable": false, } ],
        "columns": [   
            { "data": "kd_customer" },
            { "data": "nm_customer" },
            { "data": "address" },
            { "data": "phone" },
            { "data": "pic" },
            { "data": "email" },
            { 
                "data": "kd_customer" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><button data-toggle="modal" onclick="getDataCust(\''+full.kd_customer+'\',\''+full.nm_customer+'\',\''+full.address+'\',\''+full.phone+'\',\''+full.pic+'\',\''+full.email+'\')" class="btn btn-default btn-circle"  data-target="#modalCustomer-E"><span data-toggle="tooltip" data-placement="left" title="Edit" class="fa fa-edit"></span></button> <button onclick="deleteCustomer(\''+urlDelete+'\',\''+urlLoading+'\',\''+full.kd_customer+'\',\''+full.nm_customer+'\')" class="btn btn-danger btn-circle" href="#" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></button></div>'
                }  
            },     
        ],    
    }); 
}

function storeCustomer(url,urlLoading,flag){
    if($('#formCustomerEdit').valid() && $('#formCustomerAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                if (flag == "Edit"){
                    var formData = new FormData($("#formCustomerEdit")[0]);
                    text = 'Update Data Customer';
                }else if (flag == "Insert"){
                    var formData = new FormData($("#formCustomerAdd")[0]);
                    text = 'Saving Data Customer';
                }
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then(() => location.reload());
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getDataCust(id,name,address,phone,pic,email){
    $('#custID').val(id);
    $('#custName').val(name);
    $('#address').val(address);
    $('#phone').val(phone);
    $('#pic').val(pic);
    $('#email').val(email);
}

function deleteCustomer(url,urlLoading,customerId,customerName){
    var content = document.createElement("div");
        content.innerHTML = "<font color='gray'> Are you sure to delete <strong>"+ customerName +"</strong> from list customer?</font>";
    swal({
        title: "Delete",
        content: content,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                beforeSend: function(){
                    swal({
                        title: "Loading...",
                        text: "Please wait!",
                        icon: urlLoading,      
                        button: false
                    });
                },
                data: {
                    'custID' : customerId
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        type = "success";
                        message = response.message;
                    }else{
                        type = "error";
                        message = response.message;                       
                    }
                    var content = document.createElement("div");
                    content.innerHTML = "<font color='gray'> Customer <strong>"+ customerName +"</strong> deleted</font>";
                    swal({
                        title: "Delete Data",
                        content: content,
                        icon: type
                    })
                    .then(() => location.reload());
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Delete Customer Error !", "Please try again", "error");
                }
            });
        }else{
          swal("Cancelled", "Delete Data Canceled!", "error")
          .then(() => location.reload());
        }
    });
}