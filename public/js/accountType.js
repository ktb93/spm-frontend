$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formAccTypeAdd').validate();
    $('#formAccTypeEdit').validate();
});

function getAccType(url,urlLoading,urlDelete){
    var table =  $('#AccTypeTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ { "targets"  : 2, "orderable": false, } ],
        "columns": [   
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "kd_tipe_akun" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><button data-toggle="modal" onclick="getDataAccType(\''+full.kd_tipe_akun+'\',\''+full.tipe_akun+'\')" class="btn btn-default btn-circle"  data-target="#modalAccType-E"><span data-toggle="tooltip" data-placement="left" title="Edit" class="fa fa-edit"></span></button> <button onclick="deleteAccType(\''+urlDelete+'\',\''+urlLoading+'\',\''+full.kd_tipe_akun+'\',\''+full.tipe_akun+'\')" class="btn btn-danger btn-circle" href="#" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></button></div>'
                }  
            },     
        ],    
    }); 
}

function storeAccType(url,urlLoading,flag){
    if($('#formAccTypeEdit').valid() && $('#formAccTypeAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                if (flag == "Edit"){
                    var formData = new FormData($("#formAccTypeEdit")[0]);
                    text = 'Update Data Account Type';
                }else if (flag == "Insert"){
                    var formData = new FormData($("#formAccTypeAdd")[0]);
                    text = 'Saving Data Account Type';
                }
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then(() => location.reload());
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getDataAccType(id,name){
    $('#accTypeID').val(id);
    $('#accType').val(name);
}

function deleteAccType(url,urlLoading,accTypeId,accType){
    var content = document.createElement("div");
        content.innerHTML = "<font color='gray'> Are you sure to delete <strong> "+ accType +"</strong> from list account type?</font>";
    swal({
        title: "Delete",
        content: content,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                beforeSend: function(){
                    swal({
                        title: "Loading...",
                        text: "Please wait!",
                        icon: urlLoading,      
                        button: false
                    });
                },
                data: {
                    'accTypeID' : accTypeId
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        type = "success";
                        message = response.message;
                    }else{
                        type = "error";
                        message = response.message;                       
                    }
                    var content = document.createElement("div");
                    content.innerHTML = "<font color='gray'> Account Type<strong> "+ accType +"</strong> deleted</font>";
                    swal({
                        title: "Delete Data",
                        content: content,
                        icon: type
                    })
                    .then(() => location.reload());
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Delete Account Type Error !", "Please try again", "error");
                }
            });
        }else{
          swal("Cancelled", "Delete Data Canceled!", "error")
          .then(() => location.reload());
        }
    });
}