
function backPlanning(url,projectID,flag){
    if(flag == 'view'){
        window.location.replace(url + '?planningProjectID=' + projectID)
    }else{
        swal({
            title: "Completed",
            text: "Are you sure you want to finish planning?",
            icon : "warning",
            buttons: ["Cancel", "Yes, I am"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                swal({
                    title: "Completed",
                    text: "Creating Planning Expense",
                    icon: "success"
                })
                .then(() => window.location.replace(url + '?planningProjectID=' + projectID));
            }else{
                swal("Cancelled", "Cancel to go previous page.", "error")
            }
        });
    }
}

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formAccount-A').validate();
    $('#formAccountPlanAdd').validate();
});

function endDateProject(url,urlProject,urlContract){
    if($('#filterStart').val() != ''){
        $("#inputDate").html('<script>$("#filterFinish").datepicker({format: "dd-mm-yyyy",autoclose: true, todayHighlight: true});</script><input type="text" class="form-control "  placeholder="End Date" value="" name="filterFinish" id="filterFinish" onchange="getProjectList(\''+url+'\',\''+urlProject+'\',\''+urlContract+'\')"  readonly>');  
        $("#refreshDate");
    }
}

function checkPlan(url, contractID){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            "contractID" : contractID
        },
        success: function(response){ // What to do if we succeed
            if(response.status == true){
                $("#detailAccount").load(urlDtlAccount);
                $("#planningID").val(response.data.kd_rab);
                $("#btnPlan").hide();
           }
        }
    });

}

function getProjectList(url,urlProject,urlContract){
    type = $('#type').val();
    startDate = $('#filterStart').val();
    endDate = $('#filterFinish').val();
    var table =  $('#projectListTable').DataTable({
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            "data" :{
                "type" : type,
                "startDate" : startDate,
                "endDate" : endDate
            }         
        },
        "columnDefs": [ 
            { "targets"  : [5,6], "visible": false, }
        ],
        "columns": [   
            { 
                "data": "kd_proyek", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickProject(\''+urlProject+'\',\''+urlContract+'\',\''+full.kd_proyek+'\')">'+ full.kd_proyek +'</a>';
                }
            },
            { "data": "nm_proyek" },
            { "data": "location" },
            { "data": "plan_start" },
            { "data": "plan_finish" },
            { "data": "first_name" },
            { "data": "last_name" },
            { 
                "data": "kd_proyek" ,
                "render": function (id, type, full, meta) {                    
                    return '<div>'+ full.first_name + ' ' + full.last_name +'</div>'
                }  
            },
            { "data": "nilai_proyek" },
            
        ],    
    });
}

function pickProject(url,urlContract,projectID){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            "kd_proyek" : projectID
        },
        success: function(response){ // What to do if we succeed
            data = response.data[0];
            $("#projectID").val(data.kd_proyek);
            $("#project").val(data.nm_proyek);
            $("#location").val(data.location);
            $("#planStart-P").val(data.plan_start);
            $("#planFinish-P").val(data.plan_finish);
            $("#picName").val(data.first_name+ ' ' +data.last_name);
            $('#modalProjectList').modal('hide');

            var table =  $('#contractPlanExpenseTable').DataTable({
                "bInfo" : false,
                "bPaginate": false,
                "processing":true,
                "serverSide":true,
                "bDestroy": true,
                "ajax":{
                    "url": urlContract,
                    "type":"POST",
                    "data": {
                        'projectID' : projectID
                    },
                    "dataSrc": "data",
                    "headers": {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },         
                },
                "columnDefs": [ 
                    { "targets"  : [ 0, 1 ], "visible": false, }, 
                    { "targets"  : 7, "orderable": false, }
                ],
                "columns": [   
                    { "data": "kd_kontrak" },
                    { "data": "kd_customer" },
                    { "data": "no_kontrak" },
                    { "data": "desk_kontrak" },
                    { "data": "nm_customer" },
                    { "data": "deadline" },
                    { 
                        "data": "nilai_kontrak",
                        "render": function (id, type, full, meta) {
                        return '<div class="text-right">'+(full.nilai_kontrak).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                        }    
                    },
                    { 
                        "data": "kd_kontrak" ,
                        "render": function (id, type, full, meta) {                    
                            return '<div class="text-center"><a  href="'+urlPlan+'/'+full.kd_kontrak+'" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="left" title="Planning"><span class="fa fa-suitcase"></span></a> <a  href="'+urlViewPlan+'/'+full.kd_kontrak+'" class="btn btn-warning btn-circle" data-toggle="tooltip" data-placement="left" title="View Details"><span class="fa fa-file-text"></span></a></div>'
                        }  
                    }, 
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
            
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over all pages
                    total = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
            
                    // Update footer
                    $( api.column( 6 ).footer() ).html(
                       total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
                    );
                }    
            }); 
        },
    });
}

function getAccountList(url,urlAccType,urlAccID){
    var table =  $('#accountListTable').DataTable({
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },        
        },
        "columnDefs": [ 
            { "targets"  : [0,2], "visible": false, }
        ],
        "columns": [   
            { "data": "kd_akun" },
            { 
                "data": "nm_akun", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickAccount(\''+full.kd_akun+'\',\''+full.nm_akun+'\')">'+ full.nm_akun +'</a>';
                }
            },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },      
        ],
        "language": {
            "zeroRecords": 'No data available in table...! <a class="btn btn-primary btn-xs" onclick="addAccount(\''+urlAccType+'\',\''+urlAccID+'\')"><span class="fa  fa-external-link"></span>  Create New Account</a>',
        } 
    });
}

function addAccount(url,urlID){
    $('#modalAccountList').modal('hide');
    $('#modalAccount-A').modal('show');
    $.ajax({
        url: url,
        type: "POST",
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){
            if(response.success == true){
                var status = [];
                for(var i=0;i<response.recordsTotal;i++){
                    status.push(response.data[i]);
                }
                $("#accTypeA").empty();
                addInv(status);
                function addInv(data) {
                    // Get select
                    var select = document.getElementById('accTypeA');
                    $(select).append('<option value="">- Please Select -</option>');

                    // Add options
                    for (var i in data) {
                        $(select).append('<option value="' + data[i].kd_tipe_akun+ '" class="capitalize">'+data[i].tipe_akun+'</option>');
                    }
                }
            }else{
                swal("Error", "Get Account Type List Failed!", "error");
            }
        }
    });
    $.ajax({                     
        method: 'GET', // Type of response and matches what we said in the route
        url: urlID, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){ // What to do if we succeed
            $('#accID-A').val(response.accountID);
        },
    });
}

function pickAccount(accID,accName){
    $('#accID').val(accID);
    $('#selectAcc').val(accName);
    $('#modalAccountList').modal('hide');
}

function storePlan(url,userID){
    swal({
        title: "Start Plan",
        text: "Are you sure to create planning?",
        icon : "warning",
        buttons: ["Cancel", "Save"],
        closeModal : false
    })
    .then((isConfirm) => {
        if (isConfirm)
        {
            planID = $('#planID').val();
            contractID = $('#contractID').val();
            $.ajax({                     
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route           
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                data: {
                    'planID' : planID,
                    'contractID' : contractID,
                    'userID' : userID
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        swal({
                            title: response.message,
                            text: 'Saving Data RAB',
                            icon: 'success'
                        })
                        $("#detailAccount").load(urlDtlAccount);
                        $('#planningID').val(planID);
                        $("#btnPlan").hide();
                    }else{
                        swal({
                            title: response.message,
                            text: 'Saving Data RAB',
                            icon: 'error'
                        })
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Error Insert!", "Please try again", "error");
                }
            });
        }else{
            swal("Cancelled", "Cancel to create RAB.", "error")
        }
    });
}

function storeAccPlan(url,urlLoading,flag){
    if($('#formAccount-A').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formAccount-A")[0]);
                text = 'Saving Data Account';
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then((isConfirm) => {
                                if(isConfirm){
                                    if(type=="success"){
                                        $('#modalAccount-A').on('hidden.bs.modal', function () {
                                            $(this).find('form').trigger('reset');
                                        })
                                        $('#modalAccount-A').modal('hide');
                                        $('#modalAccountList').modal('show');
                                        $('#accountListTable').DataTable().ajax.reload();
                                    }
                                }
                            });
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getViewAccount(url,contractID){
    var table = $('#accountPlanExpenseTable-V').DataTable({
        "ordering": false,
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "data":{
                'contractID' : contractID
            },
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 2 ], "visible": false, },
        ],
        "columns": [   
            { "data": "kd_akun" },
            { "data": "nm_akun" },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "jml_rab",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.jml_rab).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            }, 
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            $( api.column( 4 ).footer() ).html(
               total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }  
    });
}