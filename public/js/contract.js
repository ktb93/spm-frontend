
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formContractAdd').validate();
    $('#formContractTmp').validate();
    $('#idContract').hide();
    $('#idCustomer').hide();
});

$('#deadline').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});

$('#deadline-E').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});

function validationDeadline() {
    var start = $('#planStart').val().slice(0, 10).split('-');
    var finish = $('#planFinish').val().slice(0, 10).split('-');
    var deadline = $('#deadline').val().slice(0, 10).split('-');
    var startDate = new Date(start[1] +'/'+ start[0] +'/'+ start[2]);
    var finishDate = new Date(finish[1] +'/'+ finish[0] +'/'+ finish[2]);
    var deadDate = new Date(deadline[1] +'/'+ deadline[0] +'/'+ deadline[2]);
    if (start == "" || finish == ""){
        swal("", "Start Date and Finish Date Must Be Filled First", "error")
        .then((isConfirm) => {
            if(isConfirm){
                $('#deadline').val("");
                $('#modalContract-A').modal('hide');
                document.getElementById("planStart").focus();
            }
        });
    }
    if (finishDate < deadDate || startDate > deadDate){
        swal("", "Deadline Must Be Between Start Date and Finish Date", "error");
        $('#deadline').val("");
    }
}

function validDeadline() {
    var start = $('#pStart').val().slice(0, 10).split('-');
    var finish = $('#pFinish').val().slice(0, 10).split('-');
    var deadline = $('#deadline').val().slice(0, 10).split('-');
    var deadlineEdit = $('#deadline-E').val().slice(0, 10).split('-');
    var startDate = new Date(start[1] +'/'+ start[0] +'/'+ start[2]);
    var finishDate = new Date(finish[1] +'/'+ finish[0] +'/'+ finish[2]);
    var deadDate = new Date(deadline[1] +'/'+ deadline[0] +'/'+ deadline[2]);
    var deadDateEdit = new Date(deadlineEdit[1] +'/'+ deadlineEdit[0] +'/'+ deadlineEdit[2]);
    if (finishDate < deadDate || startDate > deadDate){
        swal("", "Deadline Must Be Between Start Date and Finish Date", "error");
        $('#deadline').val("");
    }
    if (finishDate < deadDateEdit || startDate > deadDateEdit){
        swal("", "Deadline Must Be Between Start Date and Finish Date", "error");
        $('#deadline').val("");
    }
}

//Using live decimal when input "Transaction Amount"
(function($, undefined) {
    "use strict";
    // When ready.
    $(function() {
        var $form = $( "#formContractTmp" );
        var $input = $form.find( "#contractVal" );
        $input.on( "keyup", function( event ) { 
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }
            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }
            var $this = $( this );
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );
        
    });
})(jQuery);

function closeModal(flag){
    $('#modalCustomerList').hide();
    if(flag == 'Edit'){
        $('#modalContract-E').show();
    }else{
        $('#modalContract-A').show();
    }

}

function pickCustomer(customerID,customerName,flag){
    $('#modalCustomerList').hide();
    if(flag == 'Edit'){
        $('#modalContract-E').show();
        $('#custID-E').val(customerID);
        $('#custName-E').val(customerName);
    }else{
        $('#modalContract-A').show();
        $('#custID').val(customerID);
        $('#custName').val(customerName);
     
    }

}

function getCustList(url,flag){
    if(flag == 'Edit'){
        $('#modalContract-E').hide();
    }else{
        $('#modalContract-A').hide();
    }
    $('#modalCustomerList').show();
    var table =  $('#custTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "bLengthChange": false,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ { "targets"  : 0, "visible": false, } ],
        "columns": [
            { "data": "kd_customer" },   
            { 
                "data": "nm_customer", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickCustomer(\''+full.kd_customer+'\',\''+full.nm_customer+'\',\''+flag+'\')">'+ full.nm_customer +'</a>';
                }
            },
            { "data": "address" },     
        ],    
    }); 
}

function fContractID(url){
    $.ajax({                     
        method: 'GET', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){ // What to do if we succeed
            contractID = response.contractID;
            if(contractID == 'CO00001'){
                contractID = $('#ID').val();
            }
            $('#contractID').val(contractID);
        },
        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            swal("Error Insert!", "Please try again", "error");
        }
    });
}

function getTmpContract(url,urlDelete){
    var table =  $('#contractTmpTable').DataTable({
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 1 ], "visible": false, }, 
            { "targets"  : 7, "orderable": false, }
        ],
        "columns": [   
            { "data": "kd_kontrak" },
            { "data": "kd_customer" },
            { "data": "no_kontrak" },
            { "data": "desk_kontrak" },
            { "data": "nm_customer" },
            { "data": "deadline" },
            { 
                "data": "nilai_kontrak",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.nilai_kontrak).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }  
            },
            { 
                "data": "kd_kontrak" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><a onclick="delTmpContract(\''+urlDelete+'\',\''+full.kd_kontrak+'\')" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></a></div>'
                }  
            }, 
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
    
            // Total over this page
            // pageTotal = api
            //     .column( 6, { page: 'current'} )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
    
            // Update footer
            $( api.column( 6 ).footer() ).html(
               total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }        
    }); 
}

function TmpContract(url){
    if($('#formContractTmp').valid()){
        var formData = new FormData($("#formContractTmp")[0]);
        $.ajax({                     
            method: 'POST', // Type of response and matches what we said in the route
            url: url, // This is the url we gave in the route           
            headers:    
            {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            data: formData,
            processData: false,
            contentType: false,
            success: function(){ // What to do if we succeed
                $('#modalContract-A').on('hidden.bs.modal', function () {
                    $(this).find('form').trigger('reset');
                })
                $('#modalContract-A').modal('hide');
                $('#contractTmpTable').DataTable().ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log(JSON.stringify(jqXHR));
                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                swal("Error Insert!", "Please try again", "error");
            }
        });
    }
}

function delTmpContract(url,contractID){
    $.ajax({
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            'contractID' : contractID
        },
        success: function(response){ // What to do if we succeed
            $('#contractTmpTable').DataTable().ajax.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
            console.log(JSON.stringify(jqXHR));
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
            swal("Delete Customer Error !", "Please try again", "error");
        }
    });
}

//Using live decimal when input "Transaction Amount"
(function($, undefined) {
    "use strict";
    // When ready.
    $(function() {
        var $form = $( "#formContractAdd" );
        var $input = $form.find( "#contractVal" );
        $input.on( "keyup", function( event ) { 
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }
            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }
            var $this = $( this );
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );
        
    });
})(jQuery);

//Using live decimal when input "Transaction Amount"
(function($, undefined) {
    "use strict";
    // When ready.
    $(function() {
        var $form = $( "#formContractEdit" );
        var $input = $form.find( "#contractVal-E" );
        $input.on( "keyup", function( event ) { 
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }
            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }
            var $this = $( this );
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );
        
    });
})(jQuery);

function getContract(url,projectID,urlContract,urlCloseContract){
    var table =  $('#contractTable').DataTable({
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "data": {
                'projectID' : projectID
            },
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 1 ], "visible": false, }, 
            { "targets"  : 8, "orderable": false, }
        ],
        "columns": [   
            { "data": "kd_kontrak" },
            { "data": "kd_customer" },
            { "data": "no_kontrak" },
            { "data": "desk_kontrak" },
            { "data": "nm_customer" },
            { "data": "deadline" },
            { 
                "data": "nilai_kontrak",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.nilai_kontrak).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
            { 
                "data": "status" ,
                "render": function (id, type, full, meta) {
                    if(full.status == 'Finish'){
                        return '<div class="text-center" style="color:green;">Finish</div>'
                    }else if(full.status == 'Cancel'){
                        return '<div class="text-center" style="color:red;">Cancel</div>'
                    }else{
                        return '<div class="text-center">Progress</div>'
                    }

                }  
            }, 
            { 
                "data": "kd_kontrak" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><a data-toggle="modal" data-target="#modalContract-E" onclick="dtlContract(\''+urlContract+'\',\''+full.kd_kontrak+'\')" class="btn btn-default btn-circle" data-toggle="tooltip" data-placement="left" title="Update"><span class="fa fa-edit"></span></a> <a data-toggle="modal" data-target="#modalContract-S" onclick="dtlContract(\''+urlContract+'\',\''+full.kd_kontrak+'\')" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="left" title="Status"><span class="fa fa-archive"></span></a></div>'
                }  
            }, 
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
    
            // Total over this page
            // pageTotal = api
            //     .column( 6, { page: 'current'} )
            //     .data()
            //     .reduce( function (a, b) {
            //         return intVal(a) + intVal(b);
            //     }, 0 );
    
            // Update footer
            $( api.column( 6 ).footer() ).html(
               total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }    
    }); 
}

function dtlContract(url,contractID){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            'kd_kontrak' : contractID
        },
        success: function(response){ // What to do if we succeed
            data = response.data[0];
            $('#contractID-E').val(data.kd_kontrak);
            $('#contractID-S').val(data.kd_kontrak);
            $('#contractNo-E').val(data.no_kontrak);
            $('#contract-E').val(data.desk_kontrak);
            $('#custID-E').val(data.kd_customer);
            $('#custName-E').val(data.nm_customer);
            $('#contractVal-E').val(data.nilai_kontrak);
            $('#deadline-E').val(data.deadline);
            $('#status').val(data.status);
        },
    });
}

function updateContract(url,urlLoading){
    if($('#formContractEdit').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formContractEdit")[0]);
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Saving Data Contract',
                                icon: 'success'
                            })
                            .then(() => location.reload());
                        }else{
                            swal({
                                title: response.message,
                                text: 'Saving Data Contract',
                                icon: 'error'
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function storeContract(url){
    if($('#formContractAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure? Because if save this data, it can't be delete",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formContractAdd")[0]);
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Saving Data Contract',
                                icon: 'success'
                            })
                            .then(() => location.reload());
                        }else{
                            swal({
                                title: response.message,
                                text: 'Saving Data Contract',
                                icon: 'error'
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
            }
        });
    }
}

function closeContract(url){
    status = $('#status').val();
    contractID = $('#contractID-S').val();
    swal({
        title: "Save",
        text: "Are you sure you want to change status "+status+" ?",
        icon : "warning",
        buttons: ["Cancel", "Yes, I am"],
        closeModal : false
    })
    .then((isConfirm) => {
        if (isConfirm)
        {
            $.ajax({                     
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route           
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                data: {
                    "contractID" : contractID,
                    "status" : status
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        swal({
                            title: status,
                            text: 'Change Status Contract',
                            icon: 'success'
                        })
                        .then(() => location.reload());
                    }else{
                        swal({
                            title: status,
                            text: 'Change Status Contract',
                            icon: 'error'
                        })
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Error Insert!", "Please try again", "error");
                }
            });
        }else{
            swal("Cancelled", "Save data has been cancel.", "error")
            .then(() => location.reload());
        }
    });
}

function arrayContract(contractID, contractNo, contract, custID, custName, contractVal, deadline){
    if($('#formContractAdd').valid()){
        $("#contractTable tbody").append('<tr><td>' +contractID+ '</td><td>' +contractNo+ '</td><td>' +contract+ '</td><td>' +custID+ '</td><td>' +custName+ '</td><td>' +contractVal+ '</td><td>' +deadline+ '</td></tr>')
    }
}


