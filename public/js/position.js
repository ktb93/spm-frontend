$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formPositionAdd').validate();
    $('#formPositionEdit').validate();
});

function getPosition(url,urlLoading,urlDelete){
    var table =  $('#positionTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ { "targets"  : 2, "orderable": false, } ],
        "columns": [   
            { "data": "id_position" },
            { "data": "position" },
            { 
                "data": "id_position" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><button data-toggle="modal" onclick="getDataPos(\''+full.id_position+'\',\''+full.position+'\')" class="btn btn-default btn-circle"  data-target="#modalPosition-E"><span data-toggle="tooltip" data-placement="left" title="Edit" class="fa fa-edit"></span></button> <button onclick="deletePosition(\''+urlDelete+'\',\''+urlLoading+'\',\''+full.id_position+'\',\''+full.position+'\')" class="btn btn-danger btn-circle" href="#" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></button></div>'
                }  
            },     
        ],    
    }); 
}

function storePosition(url,urlLoading,flag){
    if($('#formPositionEdit').valid() && $('#formPositionAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                if (flag == "Edit"){
                    var formData = new FormData($("#formPositionEdit")[0]);
                    text = 'Update Data Position';
                }else if (flag == "Insert"){
                    var formData = new FormData($("#formPositionAdd")[0]);
                    text = 'Saving Data Position';
                }
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then(() => location.reload());
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getDataPos(id,name){
    $('#posID').val(id);
    $('#position').val(name);
}

function deletePosition(url,urlLoading,positionId,position){
    var content = document.createElement("div");
        content.innerHTML = "<font color='gray'> Are you sure to delete <strong>"+ position +"</strong> from list position?</font>";
    swal({
        title: "Delete",
        content: content,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                beforeSend: function(){
                    swal({
                        title: "Loading...",
                        text: "Please wait!",
                        icon: urlLoading,      
                        button: false
                    });
                },
                data: {
                    'posID' : positionId
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        type = "success";
                        message = response.message;
                    }else{
                        type = "error";
                        message = response.message;                       
                    }
                    var content = document.createElement("div");
                    content.innerHTML = "<font color='gray'> Position <strong>"+ position +"</strong> deleted</font>";
                    swal({
                        title: "Delete Data",
                        content: content,
                        icon: type
                    })
                    .then(() => location.reload());
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Delete Position Error !", "Please try again", "error");
                }
            });
        }else{
          swal("Cancelled", "Delete Data Canceled!", "error")
          .then(() => location.reload());
        }
    });
}