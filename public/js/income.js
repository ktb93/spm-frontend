$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

function getIncome(url,urlAdd){
    console.log($("#projectID").val());
    var table =  $('#contractIncomeTable').DataTable({
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            "data" :{
                "projectID" : $("#projectID").val(),
            }     
        },
        "columnDefs": [ 
                { "targets"  : 8, "orderable": false, },
                { "targets"  : [0,1], "visible": false, }
        ],
        "columns": [   
            { "data": "kd_kontrak" },
            { "data": "kd_customer" },
            { "data": "no_kontrak" },
            { "data": "desk_kontrak" },
            { "data": "nm_customer" },
            { "data": "nilai_kontrak" },
            { "data": "jml_pendapatan" },
            { "data": "balance" },
            { 
                "data": "kd_kontrak" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><a href="'+urlAdd+'/'+full.kd_kontrak+'" class="btn btn-success"><span data-toggle="tooltip" data-placement="left" title="Income" class="fa fa-plus"></span></a>'
                }  
            },
        ],    
    }); 
}

function getProjectInc(url,urlProject,urlGet){
    type = $('#type').val();
    startDate = $('#filterStart').val();
    endDate = $('#filterFinish').val();
    var table =  $('#projectListTable').DataTable({
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            "data" :{
                "type" : type,
                "startDate" : startDate,
                "endDate" : endDate
            }         
        },
        "columnDefs": [ 
            { "targets"  : [5,6], "visible": false, }
        ],
        "columns": [   
            { 
                "data": "kd_proyek", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickFilter(\''+urlProject+'\',\''+full.kd_proyek+'\',\''+urlGet+'\')">'+ full.kd_proyek +'</a>';
                }
            },
            { "data": "nm_proyek" },
            { "data": "location" },
            { "data": "plan_start" },
            { "data": "plan_finish" },
            { "data": "first_name" },
            { "data": "last_name" },
            { 
                "data": "kd_proyek" ,
                "render": function (id, type, full, meta) {                    
                    return '<div>'+ full.first_name + ' ' + full.last_name +'</div>'
                }  
            },
            { "data": "nilai_proyek" },
            
        ],    
    });
}

function pickFilter(url,projectID,urlGet){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            "kd_proyek" : projectID
        },
        success: function(response){ // What to do if we succeed
            data = response.data[0];
            $("#projectID").val(data.kd_proyek);
            $("#selectPro").val(data.nm_proyek);
            getIncome(urlGet);
            $('#modalProjectInc').modal('hide');
        },
    });
}