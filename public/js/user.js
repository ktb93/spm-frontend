$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formUserAdd').validate({
        rules: {
            email: {
                email: true
            },
            repassword: {
                equalTo: '[name="password"]'
            }
        },
        messages: {
            repassword: {
                equalTo: "Re-type doesn't match password"
            }
        }           
    });
    $('#formUserEdit').validate({
        rules: {
            email: {
                email: true
            },
            repassword: {
                equalTo: '[name="password"]'
            }
        },
        messages: {
            repassword: {
                equalTo: "Re-type doesn't match password"
            }
        }          
    });

    $('#password').strengthMeter('progressBar', {
        container: $('#password-strength'),
        hierarchy: {
            '0': 'progress-bar-danger progress-bar-striped active',
            '10': 'progress-bar-warning progress-bar-striped active',
            '50': 'progress-bar-success progress-bar-striped active'
        }
    });
});

function getUser(url,urlLoading,urlDelete,urlPosition){
    var table =  $('#userTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : 7, "orderable": false, },
            { "targets"  : 5, "visible": false, }
        ],
        "columns": [   
            { "data": "id_user" },
            { "data": "first_name" },
            { "data": "last_name" },
            { "data": "username" },
            { "data": "password" },
            { "data": "id_position" },
            { "data": "position" },
            { "data": "email" },
            { 
                "data": "kd_user" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><button data-toggle="modal" onclick="getDataUser(\''+full.id_user+'\',\''+full.first_name+'\',\''+full.last_name+'\',\''+full.username+'\',\''+full.id_position+'\',\''+full.position+'\',\''+full.email+'\',\''+urlPosition+'\')" class="btn btn-default btn-circle"  data-target="#modalUser-E"><span data-toggle="tooltip" data-placement="left" title="Edit" class="fa fa-edit"></span></button> <button onclick="deleteUser(\''+urlDelete+'\',\''+urlLoading+'\',\''+full.id_user+'\',\''+full.username+'\')" class="btn btn-danger btn-circle" href="#" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></button></div>'
                }  
            },     
        ],    
    }); 
}

function storeUser(url,urlLoading,flag){
    if($('#formUserEdit').valid() && $('#formUserAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                if (flag == "Edit"){
                    var formData = new FormData($("#formUserEdit")[0]);
                    text = 'Update Data User';
                }else if (flag == "Insert"){
                    var formData = new FormData($("#formUserAdd")[0]);
                    text = 'Saving Data User';
                }
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then(() => location.reload());
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getDataUser(id,fname,lname,user,posID,position,email,url){
    $('#userID').val(id);
    $('#firstname').val(fname);
    $('#lastname').val(lname);
    $('#username').val(user);
    $('#email').val(email);

    $.ajax({
        url: url,
        type: "POST",
        data:{
            "posID":posID
        },
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){
            if(response.success == true){
                var status = [];
                for(var i=0;i<response.recordsTotal;i++){
                    status.push(response.data[i]);
                }
                $("#positionE").empty();
                addInv(status);
                function addInv(data) {
                    // Get select
                    var select = document.getElementById('positionE');
                    $(select).append('<option value="' +posID+ '">' +position+ '</option>');

                    // Add options
                    for (var i in data) {
                        $(select).append('<option value="' + data[i].id_position+ '" class="capitalize">'+data[i].position+'</option>');
                    }
                }
            }else{
                swal("Error", "Get Positon List Failed!", "error");
            }
        }
    });

}

function getPositionUser(url){
    $.ajax({
        url: url,
        type: "POST",
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){
            if(response.success == true){
                var status = [];
                for(var i=0;i<response.recordsTotal;i++){
                    status.push(response.data[i]);
                }
                $("#positionA").empty();
                addInv(status);
                function addInv(data) {
                    // Get select
                    var select = document.getElementById('positionA');
                    $(select).append('<option value="">- Please Select -</option>');

                    // Add options
                    for (var i in data) {
                        $(select).append('<option value="' + data[i].id_position+ '" class="capitalize">'+data[i].position+'</option>');
                    }
                }
            }else{
                swal("Error", "Get Positon List Failed!", "error");
            }
        }
    });
}

function deleteUser(url,urlLoading,userId,userName){
    var content = document.createElement("div");
        content.innerHTML = "<font color='gray'> Are you sure to delete <strong>"+ userName +"</strong> from list user?</font>";
    swal({
        title: "Delete",
        content: content,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                beforeSend: function(){
                    swal({
                        title: "Loading...",
                        text: "Please wait!",
                        icon: urlLoading,      
                        button: false
                    });
                },
                data: {
                    'userID' : userId
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        type = "success";
                        message = response.message;
                    }else{
                        type = "error";
                        message = response.message;                       
                    }
                    var content = document.createElement("div");
                    content.innerHTML = "<font color='gray'> User <strong>"+ userName +"</strong> deleted</font>";
                    swal({
                        title: "Delete Data",
                        content: content,
                        icon: type
                    })
                    .then(() => location.reload());
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Delete User Error !", "Please try again", "error");
                }
            });
        }else{
          swal("Cancelled", "Delete Data Canceled!", "error")
          .then(() => location.reload());
        }
    });
}

// Password Strength with Parameter

// $('#password-parameter').keyup(function() {
//     password = $('#password').val()
//     console.log(password)
//     if ($('#password').val() != ''){
//         $('#title').html('Strength: ')
//         $('#title').addClass('col-md-3 title')
//         $('#result').html(checkStrength($('#password').val()))
//     }
// })
// function checkStrength(password) {
//     var strength = 0
//     if (password.length < 6){
//         $('#result').removeClass()
//         $('#result').addClass('short')
//         return 'Too Short'
//     }
//     if (password.length > 7) strength += 1
//     // If password contains both lower and uppercase characters, increase strength value.
//     if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
//     // If it has numbers and characters, increase strength value.
//     if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
//     // If it has one special character, increase strength value.
//     if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
//     // If it has two special characters, increase strength value.
//     if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
//     // Calculated strength value, we can return messages
//     // If value is less than 2
//     if (strength < 2){
//         $('#result').removeClass()
//         $('#result').addClass('weak')
//         return 'Weak'
//     }else if(strength == 2){
//         $('#result').removeClass()
//         $('#result').addClass('good')
//         return 'Good'
//     }else{
//         $('#result').removeClass()
//         $('#result').addClass('strong')
//         return 'Strong'
//     }
// }