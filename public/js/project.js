$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formProjectAdd').validate();
});

$('#filterStart').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});


$('#filterFinish').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});

$('#planStart').datepicker({
    startDate : new Date(),
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});


$('#planFinish').datepicker({
    startDate : new Date(),
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});

$('#actualStart').datepicker({
    startDate : new Date(),
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});


$('#actualFinish').datepicker({
    startDate : new Date(),
    todayHighlight: true,
    format: "dd-mm-yyyy",
    autoclose: true
});

function validationDate() {
    var start = $('#planStart').val().slice(0, 10).split('-');
    var finish = $('#planFinish').val().slice(0, 10).split('-');   
    var startDate = new Date(start[1] +'/'+ start[0] +'/'+ start[2]);
    var endDate = new Date(finish[1] +'/'+ finish[0] +'/'+ finish[2]);
    if (startDate > endDate){
        swal("", "Finish Date Must Be Greater Than Start Date", "error");
        $('#planFinish').val("");
    }
}

function validationClosing() {
    var start = $('#actualStart').val().slice(0, 10).split('-');
    var finish = $('#actualFinish').val().slice(0, 10).split('-');   
    var startDate = new Date(start[1] +'/'+ start[0] +'/'+ start[2]);
    var endDate = new Date(finish[1] +'/'+ finish[0] +'/'+ finish[2]);
    if (startDate > endDate){
        swal("", "Finish Date Must Be Greater Than Start Date", "error");
        $('#actualFinish').val("");
    }
}

function endDateInput(url,urlView,refresh){
    if($('#filterStart').val() != ''){
        $("#inputDate").html('<script>$("#filterFinish").datepicker({format: "dd-mm-yyyy",autoclose: true, todayHighlight: true});</script><input type="text" class="form-control "  placeholder="End Date" value="" name="filterFinish" id="filterFinish" onchange="getProject(\''+url+'\',\''+urlView+'\')"  readonly>');  
        $("#refreshDate").html('<a  onclick="getProject(\''+url+'\',\''+urlView+'\',\''+refresh+'\')" class="btn btn-default btn-circle"><span class="fa fa-refresh"></span></a>');
    }
}

function getProject(url,urlView,refresh){
    if(refresh == 'refresh'){
        console.log('test');
        $('#filterStart').val('');
        $("#inputDate").html('');
        $("#refreshDate").html('');
    }
    type = $('#type').val();
    startDate = $('#filterStart').val();
    endDate = $('#filterFinish').val();
    var table =  $('#projectTable').DataTable({
        "order": [[ 0, "desc" ]],
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            "data" :{
                "type" : type,
                "startDate" : startDate,
                "endDate" : endDate
            }         
        },
        "columnDefs": [ 
                { "targets"  : 6, "orderable": false, },
                { "targets"  : 7, "visible": false, }
        ],
        "columns": [   
            { "data": "kd_proyek" },
            { "data": "nm_proyek" },
            { "data": "location" },
            { "data": "plan_start" },
            { "data": "plan_finish" },
            { 
                "data": "progress" ,
                "render": function (id, type, full, meta) {
                    if(full.status == 'Closed'){
                        return '<div class="text-center"><h4>Closed</h4></div>'
                    }else if(full.status == 'Cancel'){
                        return '<div class="text-center" style="color:red;"><h4>Cancel</h4></div>'
                    }else{
                        return '<div class="progress progress-striped"><div class="progress-bar progress-bar-success" role="progressbar" style="width: '+full.progress+'%;">'+full.progress+'%</div></div>'
                    }

                }  
            },  
            { 
                "data": "kd_proyek" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><a href="'+urlView+'/'+full.kd_proyek+'" class="btn btn-warning"><span data-toggle="tooltip" data-placement="left" title="View Details" class="fa fa-file-text"></span></a>'
                }  
            },
            { "data": "status" },     
        ],    
    }); 
}

function storeProject(url,urlLoading){
    if($('#formProjectAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formProjectAdd")[0]);
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Saving Data Project',
                                icon: 'success'
                            })
                            .then(() => window.location.replace("project"));
                        }else{
                            swal({
                                title: response.message,
                                text: 'Saving Data Project',
                                icon: 'error'
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error");
            }
        });
    }
}

function updateProject(url,urlLoading){
    if($('#formProjectEdit').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formProjectEdit")[0]);
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Saving Data Project',
                                icon: 'success'
                            })
                            .then(() => location.reload());
                        }else{
                            swal({
                                title: response.message,
                                text: 'Saving Data Project',
                                icon: 'error'
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function closeProject(url,urlLoading){
    if($('#formProjectC').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to close this project?",
            icon : "warning",
            buttons: ["Cancel", "Yes, I am"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                var formData = new FormData($("#formProjectC")[0]);
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Closing Project',
                                icon: 'success'
                            })
                            .then(() => location.reload());
                        }else{
                            swal({
                                title: response.message,
                                text: 'Closing Project',
                                icon: 'error'
                            })
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
            }
        });
    }
}

function closeModalUser(){
    $('#modalUserList').hide();
    $('#modalProject-E').show();
}

function pickUser(userID,firstName, lastName){
    $('#userID').val(userID);
    $('#fullName').val(firstName+ ' ' +lastName);
    $('#modalUserList').hide();
    $('#modalProject-E').show();
}

function userProjectList(url){
    $('#modalUserList').show();
    $('#modalProject-E').hide();
    var table =  $('#userProjectTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "bLengthChange": false,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ { "targets"  : [0, 1, 2], "visible": false, } ],
        "columns": [
            { "data": "id_user" },
            { "data": "first_name" },
            { "data": "last_name" },   
            { 
                "data": "id_user", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickUser(\''+full.id_user+'\',\''+full.first_name+'\',\''+full.last_name+'\')">'+ full.first_name + ' ' + full.last_name +'</a>';
            }
            },
            { "data": "position" },     
        ],    
    }); 
}

function cancelProject(userID,url,urlLoading){
    projectID = $("#projectID").val();
    swal({
        title: "Cancel Project",
        text: "Are you sure you want to cancel this project?",
        icon : "warning",
        buttons: ["Cancel", "Yes, I am"],
        closeModal : false
    })
    .then((isConfirm) => {
        if (isConfirm)
        {
           swal({
                 title: "Reason",
                content: "input",
                })
                .then((value) => {
                    if (value == '') {
                        swal("","You must input reject reason!","error");
                    }else{
                        $.ajax({                     
                            method: 'POST', // Type of response and matches what we said in the route
                            url: url, // This is the url we gave in the route           
                            headers:    
                            {   
                                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                            },
                            beforeSend: function(){
                                swal({
                                    title: "Loading...",
                                    text: "Please wait!",
                                    icon: urlLoading,      
                                    button: false
                                });
                            },
                            data: {
                                "cancelBy" : userID,
                                "note" : value,
                                "projectID" : projectID
                            },
                            success: function(response){ // What to do if we succeed
                                if(response.success == true){
                                    swal({
                                        title: response.message,
                                        text: 'Cancel Project',
                                        icon: 'success'
                                    })
                                    .then(() => location.reload());
                                }else{
                                    swal({
                                        title: response.message,
                                        text: 'Cancel Project',
                                        icon: 'error'
                                    })
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                                console.log(JSON.stringify(jqXHR));
                                console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                                swal("Error Insert!", "Please try again", "error");
                            }
                        });
                    }
                });
        }else{
            swal("Cancelled", "Project still on progress.", "error")
        }
    });
}

function filterProject(){
    type = $('#TYPE').val();
    console.log(type);
    if(type == 'All' || type == 'kd_proyek' || type == 'nm_proyek' || type == 'location'){
        console.log('mama')
        $("#valueInput").html('<div class="col-sm-3"><input type="text" class="form-control "  placeholder="Search" value="" name="filterStart" id="filterStart" onkeypress=""></div>')
    }else if(type == 'plan_start' || type == 'plan_finish'){
        $("#valueInput").html('<script>$("#filterStart").datepicker({format: "dd-mm-yyyy",autoclose: true});$("#filterFinish").datepicker({format: "dd-mm-yyyy",autoclose: true});</script><div class="col-sm-3"><input type="text" class="form-control "  placeholder="Start Date" value="" name="filterStart" id="filterStart" onkeypress="" ></div><div class="col-sm-3"><input type="text" class="form-control "  placeholder="End Date" value="" name="filterFinish" id="filterFinish" onkeypress="" ></div>')
    }else if(type == 'status'){
        $("#valueInput").html('<div class="col-sm-2"><input type="radio" name="progress" id="status" value="Progress"> On Progress</div><div class="col-sm-1"><input type="radio" name="closed" value="Closed" id="status"> Closed</div><div class="col-sm-1"><input type="radio" name="cancel" value="Cancel" id="status"> Cancel</div>')
    }
    
}