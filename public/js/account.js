$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formAccountAdd').validate();
    $('#formAccountEdit').validate();
});

function getAccount(url,urlLoading,urlDelete,urlAccList){
    var table =  $('#accountTable').DataTable({
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"GET",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : 4, "orderable": false, },
            { "targets"  : 2, "visible": false, } 
        ],
        "columns": [   
            { "data": "kd_akun" },
            { "data": "nm_akun" },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "kd_akun" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><button data-toggle="modal" onclick="getDataAcc(\''+full.kd_akun+'\',\''+full.nm_akun+'\',\''+full.kd_tipe_akun+'\',\''+full.tipe_akun+'\',\''+urlAccList+'\')" class="btn btn-default btn-circle"  data-target="#modalAccount-E"><span data-toggle="tooltip" data-placement="left" title="Edit" class="fa fa-edit"></span></button> <button onclick="deleteAccount(\''+urlDelete+'\',\''+urlLoading+'\',\''+full.kd_akun+'\',\''+full.nm_akun+'\')" class="btn btn-danger btn-circle" href="#" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></button></div>'
                }  
            },     
        ],    
    }); 
}

function storeAccount(url,urlLoading,flag){
    if($('#formAccountEdit').valid() && $('#formAccountAdd').valid()){
        swal({
            title: "Save",
            text: "Are you sure you want to save this data?",
            icon : "warning",
            buttons: ["Cancel", "Save"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                if (flag == "Edit"){
                    var formData = new FormData($("#formAccountEdit")[0]);
                    text = 'Update Data Account';
                }else if (flag == "Insert"){
                    var formData = new FormData($("#formAccountAdd")[0]);
                    text = 'Saving Data Account';
                }
                $.ajax({                     
                    method: 'POST', // Type of response and matches what we said in the route
                    url: url, // This is the url we gave in the route           
                    headers:    
                    {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },
                    beforeSend: function(){
                        swal({
							title: "Loading...",
							text: "Please wait!",
							icon: urlLoading,      
							button: false
                        });
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response){ // What to do if we succeed
                        if(response.success == true){
                            type = "success";
                            message = response.message;
                        }else{
                            type = "error";
                            message = response.message;
                        }
                        swal({
                                title: message,
                                text: text,
                                icon: type
                            })
                            .then(() => location.reload());
                    },
                    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                        console.log(JSON.stringify(jqXHR));
                        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                        swal("Error Insert!", "Please try again", "error");
                    }
                });
            }else{
                swal("Cancelled", "Save data has been cancel.", "error")
                .then(() => location.reload());
            }
        });
    }
}

function getDataAcc(id,name,accTypeID,accType,url){
    $('#accID').val(id);
    $('#accName').val(name);

    $.ajax({
        url: url,
        type: "POST",
        data:{
            "accTypeID":accTypeID
        },
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){
            if(response.success == true){
                var status = [];
                for(var i=0;i<response.recordsTotal;i++){
                    status.push(response.data[i]);
                }
                $("#accTypeE").empty();
                addInv(status);
                function addInv(data) {
                    // Get select
                    var select = document.getElementById('accTypeE');
                    $(select).append('<option value="' +accTypeID+ '">' +accType+ '</option>');

                    // Add options
                    for (var i in data) {
                        $(select).append('<option value="' + data[i].kd_tipe_akun+ '" class="capitalize">'+data[i].tipe_akun+'</option>');
                    }
                }
            }else{
                swal("Error", "Get Account Type List Failed!", "error");
            }
        }
    });
}

function getAccTypeList(url){
    $.ajax({
        url: url,
        type: "POST",
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        success: function(response){
            if(response.success == true){
                var status = [];
                for(var i=0;i<response.recordsTotal;i++){
                    status.push(response.data[i]);
                }
                $("#accTypeA").empty();
                addInv(status);
                function addInv(data) {
                    // Get select
                    var select = document.getElementById('accTypeA');
                    $(select).append('<option value="">- Please Select -</option>');

                    // Add options
                    for (var i in data) {
                        $(select).append('<option value="' + data[i].kd_tipe_akun+ '" class="capitalize">'+data[i].tipe_akun+'</option>');
                    }
                }
            }else{
                swal("Error", "Get Account Type List Failed!", "error");
            }
        }
    });
}

function deleteAccount(url,urlLoading,accountId,accountName){
    var content = document.createElement("div");
        content.innerHTML = "<font color='gray'> Are you sure to delete <strong>"+ accountName +"</strong> from list account?</font>";
    swal({
        title: "Delete",
        content: content,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                beforeSend: function(){
                    swal({
                        title: "Loading...",
                        text: "Please wait!",
                        icon: urlLoading,      
                        button: false
                    });
                },
                data: {
                    'accID' : accountId
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        type = "success";
                        message = response.message;
                    }else{
                        type = "error";
                        message = response.message;                       
                    }
                    var content = document.createElement("div");
                    content.innerHTML = "<font color='gray'> Account <strong>"+ accountName +"</strong> deleted</font>";
                    swal({
                        title: "Delete Data",
                        content: content,
                        icon: type
                    })
                    .then(() => location.reload());
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Delete Account Error !", "Please try again", "error");
                }
            });
        }else{
          swal("Cancelled", "Delete Data Canceled!", "error")
          .then(() => location.reload());
        }
    });
}