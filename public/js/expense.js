
function backExpense(url,projectID,flag){
    if (flag == 'view'){
        window.location.replace(url + '?expenseProjectID=' + projectID)
    }else{
        swal({
            title: "Completed",
            text: "Are you sure you want to finish expense?",
            icon : "warning",
            buttons: ["Cancel", "Yes, I am"],
            closeModal : false
        })
        .then((isConfirm) => {
            if (isConfirm)
            {
                swal({
                    title: "Completed",
                    text: "Creating Expense",
                    icon: "success"
                })
                .then(() => window.location.replace(url + '?expenseProjectID=' + projectID));
            }else{
                swal("Cancelled", "Cancel to go previous page.", "error")
            }
        });
    
    }
}

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('#formAccountExpenseAdd').validate();
});

function endDateProjectExp(url,urlProject,urlContract){
    if($('#filterStart').val() != ''){
        $("#inputDate").html('<script>$("#filterFinish").datepicker({format: "dd-mm-yyyy",autoclose: true, todayHighlight: true});</script><input type="text" class="form-control "  placeholder="End Date" value="" name="filterFinish" id="filterFinish" onchange="getProjectExp(\''+url+'\',\''+urlProject+'\',\''+urlContract+'\')"  readonly>');  
        $("#refreshDate");
    }
}

function checkExpense(url, contractID){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            "contractID" : contractID
        },
        success: function(response){ // What to do if we succeed
            if(response.status == true){
                $("#newExpenseID").val(response.data.kd_biaya);
                $("#btnExpense").hide();
                $("#viewAccountPlan").hide();
                $("#detailAccount").load(urlDtlAccount);
           }
        }
    });

}

function getProjectExp(url,urlProject,urlContract){
    type = $('#type').val();
    startDate = $('#filterStart').val();
    endDate = $('#filterFinish').val();
    var table =  $('#projectListTable-E').DataTable({
        "lengthChange": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            "data" :{
                "type" : type,
                "startDate" : startDate,
                "endDate" : endDate
            }         
        },
        "columnDefs": [ 
            { "targets"  : [5,6], "visible": false, }
        ],
        "columns": [   
            { 
                "data": "kd_proyek", 
                "render": function (id, type, full, meta) {
                    return '<a onclick="pickProjectExp(\''+urlProject+'\',\''+urlContract+'\',\''+full.kd_proyek+'\')">'+ full.kd_proyek +'</a>';
                }
            },
            { "data": "nm_proyek" },
            { "data": "location" },
            { "data": "plan_start" },
            { "data": "plan_finish" },
            { "data": "first_name" },
            { "data": "last_name" },
            { 
                "data": "kd_proyek" ,
                "render": function (id, type, full, meta) {                    
                    return '<div>'+ full.first_name + ' ' + full.last_name +'</div>'
                }  
            },
            { "data": "nilai_proyek" },
            
        ],    
    });
}

function pickProjectExp(url,urlContract,projectID){
    $.ajax({                     
        method: 'POST', // Type of response and matches what we said in the route
        url: url, // This is the url we gave in the route           
        headers:    
        {   
            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
        },
        data: {
            "kd_proyek" : projectID
        },
        success: function(response){ // What to do if we succeed
            data = response.data[0];
            $("#projectID").val(data.kd_proyek);
            $("#project").val(data.nm_proyek);
            $("#location").val(data.location);
            $("#planStartExp").val(data.plan_start);
            $("#planFinishExp").val(data.plan_finish);
            $("#picName").val(data.first_name+ ' ' +data.last_name);
            $('#modalProjectList-E').modal('hide');

            var table =  $('#contractExpenseTable').DataTable({
                "bInfo" : false,
                "bPaginate": false,
                "processing":true,
                "serverSide":true,
                "bDestroy": true,
                "ajax":{
                    "url": urlContract,
                    "type":"POST",
                    "data": {
                        'projectID' : projectID
                    },
                    "dataSrc": "data",
                    "headers": {   
                        "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                    },         
                },
                "columnDefs": [ 
                    { "targets"  : [ 0, 1 ], "visible": false, }, 
                    { "targets"  : 7, "orderable": false, }
                ],
                "columns": [   
                    { "data": "kd_kontrak" },
                    { "data": "kd_customer" },
                    { "data": "no_kontrak" },
                    { "data": "desk_kontrak" },
                    { "data": "nm_customer" },
                    { "data": "deadline" },
                    { 
                        "data": "nilai_kontrak",
                        "render": function (id, type, full, meta) {
                        return '<div class="text-right">'+(full.nilai_kontrak).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                        }    
                    },
                    { 
                        "data": "kd_kontrak" ,
                        "render": function (id, type, full, meta) {                    
                            return '<div class="text-center"><a  href="'+urlExpense+'/'+full.kd_kontrak+'" class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="left" title="Expense"><span class="fa fa-suitcase"></span></a> <a  href="'+urlViewExpense+'/'+full.kd_kontrak+'" class="btn btn-warning btn-circle" data-toggle="tooltip" data-placement="left" title="View Details"><span class="fa fa-file-text"></span></a></div>'
                        }  
                    }, 
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
            
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
        
                    // Total over all pages
                    total = api
                        .column( 6 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
            
                    // Update footer
                    $( api.column( 6 ).footer() ).html(
                       total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
                    );
                }    
            }); 
        },
    });
}

function storeExpense(url,userID){
    swal({
        title: "Create Expense",
        text: "Are you sure to create expense?",
        icon : "warning",
        buttons: ["Cancel", "Yes, I am"],
        closeModal : false
    })
    .then((isConfirm) => {
        if (isConfirm)
        {
            expenseID = $('#expenseID').val();
            contractID = $('#contractID').val();
            $.ajax({                     
                method: 'POST', // Type of response and matches what we said in the route
                url: url, // This is the url we gave in the route           
                headers:    
                {   
                    "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                },
                data: {
                    'expenseID' : expenseID,
                    'contractID' : contractID,
                    'userID' : userID
                },
                success: function(response){ // What to do if we succeed
                    if(response.success == true){
                        swal({
                            title: response.message,
                            text: 'Saving Data Expense',
                            icon: 'success'
                        })
                        $("#viewAccountPlan").hide();
                        $("#detailAccount").load(urlDtlAccount);
                        $('#expenseID').val(expenseID);
                        $("#btnExpense").hide();
                    }else{
                        swal({
                            title: response.message,
                            text: 'Saving Data Expense',
                            icon: 'error'
                        })
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    console.log(JSON.stringify(jqXHR));
                    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
                    swal("Error Insert!", "Please try again", "error");
                }
            });
        }else{
            swal("Cancelled", "Cancel to create RAB.", "error")
        }
    });
}

function getViewExpense(url,contractID){
    console.log('pppp');
    $('#accountExpenseTable-V').DataTable({
        "ordering": false,
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": url,
            "type":"POST",
            "data":{
                'contractID' : contractID
            },
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 2 ], "visible": false, }
        ],
        "columns": [   
            { "data": "kd_akun" },
            { "data": "nm_akun" },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "jml_rab",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.jml_rab).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
            { 
                "data": "jml_biaya",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.jml_biaya).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
            { 
                "data": "balance",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.balance).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
        ],
        "rowCallback": function( row, data, index ) {
            if ( data['balance'] < 0 )
            {
                $('td', row).css('background-color', 'pink');
            }
        },  
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            plan = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(
               plan.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );

            expense = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 5 ).footer() ).html(
               expense.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );

            balance = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 6 ).footer() ).html(
               balance.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }  
    });
}