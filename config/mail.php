<?php

return [

    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.gmail.com'),
    'port' => env('MAIL_PORT', 587),
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'riyanalfian93@gmail.com'),
        'name' => env('MAIL_FROM_NAME', 'Testing Email'),
    ],

    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME','riyanalfian93@gmail.com'),
    'password' => env('MAIL_PASSWORD','advance123!'),
    'sendmail' => '/usr/sbin/sendmail -bs',

];
