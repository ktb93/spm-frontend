<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Account Data</b>
    </div>
    <div class="panel-body">
        <div id="expenseCreated"></div>
        <form class="form-group"  name="formAccountPlanAdd" id="formAccountPlanAdd">
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="hidden" name="accID" id="accID">
                        <input type="text" name="selectAcc" id="selectAcc"  class="form-control" value="" placeholder="Select Account..." required readonly>
                        <span class="input-group-btn">
                            <a class="btn btn-success" data-toggle="modal" data-target="#modalAccountList" onclick="getAccountList('{{config("aplikasi.apiUrl")}}/getAccount','{{config("aplikasi.apiUrl")}}/accTypeList','{{config("aplikasi.apiUrl")}}/accountID')"><span class="fa fa-list"></span></a>
                        </span>                        
                    </div>   
                </div>
                <div class="col-md-3">
                    <input type="text" name="accVal-P" id="accVal-P"  class="form-control" value="" placeholder="Account Value" required>
                </div>
                <div class="col-md-2">
                    <a class="btn btn-primary" onclick="storeDtlPLan('{{config("aplikasi.apiUrl")}}storeDetailPlan','{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->id_user}}')"><span class="fa  fa-plus"></span></a>                
                </div>
            </div>
        </form>	
        <hr>               
        <div class="col-sm-12">
        <table id="accountPlanExpenseTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
                    <th>Account ID</th>
                    <th>Account</th>
                    <th>Account Type ID</th>
                    <th>Account Type</th>
                    <th style="text-align:right">Account Value</th>
                    <th ></th>
				</tr>
            </thead>
            <tfoot>
                <tr class="success">
                    <th colspan="4"  style="text-align:right">Total :</th>
                    <th style="text-align:right"></th>
                </tr>
            </tfoot>
		</table>	
	</div>
    </div>
</div>

<script>
    var contractID = lasturl
    var urlCheckExpense = '{{config("aplikasi.apiUrl")}}checkExpense';
    var urlGetAccountStore = '{{config("aplikasi.apiUrl")}}getDetailPlan';
    var urlDeleteDtl = '{{config("aplikasi.apiUrl")}}delDtlPlan';
    
    $(document).ready(function() {
        $.ajax({                     
            method: 'POST', // Type of response and matches what we said in the route
            url: urlCheckExpense, // This is the url we gave in the route           
            headers:    
            {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            data: {
                'contractID' : contractID,
            },
            success: function(response){ // What to do if we succeed
                if(response.status == true){
                    $('#formAccountPlanAdd').hide();
                    $("#expenseCreated").html('<div class="alert alert-success"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Expense is already created so you can not change planning</b></div>');  
                }
            },
        });
    });
    
    
    
    $('#accountPlanExpenseTable').DataTable({
        "ordering": false,
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": urlGetAccountStore,
            "type":"POST",
            "data":{
                'contractID' : contractID
            },
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 2 ], "visible": false, }, 
            { "targets"  : 5, "orderable": false, }
        ],
        "columns": [   
            { "data": "kd_akun" },
            { "data": "nm_akun" },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "jml_rab",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.jml_rab).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
            { 
                "data": "kd_akun" ,
                "render": function (id, type, full, meta) {                    
                    return '<div class="text-center"><a onclick="deleteDtlPlan(\''+full.kd_akun+'\')" class="btn btn-danger btn-circle" data-toggle="tooltip" data-placement="left" title="Delete"><span class="fa fa-trash-o"></span></a></div>'
                }  
            }, 
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            $( api.column( 4 ).footer() ).html(
               total.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }  
    });

    (function($, undefined) {
        "use strict";
        // When ready.
        $(function() {
            var $form = $( "#formAccountPlanAdd" );
            var $input = $form.find( "#accVal-P" );
            $input.on( "keyup", function( event ) { 
                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                var $this = $( this );
                // Get the value.
                var input = $this.val();
                var input = input.replace(/[\D\s\._\-]+/g, "");
                        input = input ? parseInt( input, 10 ) : 0;

                        $this.val( function() {
                            return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                        } );
            } );
            
        });
    })(jQuery);

    function storeDtlPLan(url,userID){
        planID = $('#planningID').val();
        accID = $('#accID').val();
        planVal = $('#accVal-P').val();
        console.log(planID);
        $.ajax({                     
            method: 'POST', // Type of response and matches what we said in the route
            url: url, // This is the url we gave in the route           
            headers:    
            {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            data: {
                'planID' : planID,
                'accountID': accID,
                'planVal': planVal,
                'userID' : userID
            },
            success: function(response){ // What to do if we succeed
                if(response.success == true){
                    $('#accountPlanExpenseTable').DataTable().ajax.reload();
                    $('#selectAcc').val('');
                    $('#accVal-P').val('');
                }else{
                    swal({
                        title: 'Error',
                        text: response.message,
                        icon: 'error'
                    })
                }
            },
        });
    }

    function deleteDtlPlan(accountID){
        planID = $('#planningID').val();
        $.ajax({
            method: 'POST', // Type of response and matches what we said in the route
            url: urlDeleteDtl, // This is the url we gave in the route
            headers:    
            {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },
            data: {
                'planID' : planID,
                'accountID' : accountID
            },
            success: function(response){ // What to do if we succeed
                if(response.success == true){
                    $('#accountPlanExpenseTable').DataTable().ajax.reload();
                }else{
                    swal({
                        title: "Delete Data",
                        content: response.message,
                        icon: "error"
                    })                 
                }
            },
        });
    }
</script>
