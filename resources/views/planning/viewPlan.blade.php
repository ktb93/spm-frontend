@extends ('layouts.dashboard')
@section('page_heading','Planning Expense')

@section('section')
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 80%;
   max-width:1400px;
  }
}
</style>

<script type="text/javascript">
    var urlDtlAccount = {!! json_encode(url('/detailAccount')) !!};
</script>


<div class="panel panel-default"> 
    <div class="panel-body">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" onclick="backPlanning('{{url("planning")}}','{{$data->kd_proyek}}','view')"><span class="fa fa-arrow-left"></span>  Back</a>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
            <b>Detail Contract</b>
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Project ID</label>
                <div class="col-md-8">
                    <input type="text" name="projectID" id="projectID"  class="form-control" value="{{$data->kd_proyek}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Project Description</label>
                <div class="col-md-8">
                    <input type="text" name="project" id="project"  class="form-control" value="{{$data->nm_proyek}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract No</label>
                <div class="col-md-8">
                    <input type="hidden" name="contractID" id="contractID" value="{{$data->kd_kontrak}}">
                    <input type="text" name="contractNo" id="contractNo"  class="form-control" value="{{$data->no_kontrak}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract Description</label>
                <div class="col-md-8">
                    <input type="text" name="contract" id="contract" class="form-control" value="{{$data->desk_kontrak}}" readonly>   
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Customer</label>
                <div class="col-md-8">
                    <input type="text" name="customer" id="customer" class="form-control" value="{{$data->nm_customer}}" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Deadline</label>
                <div class="col-md-8">
                    <input type="text" name="deadline" id="deadline" class="form-control"  value="{{$data->deadline}}" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract Value</label>
                <div class="col-md-8">
                    <input type="text" name="contractVal" id="contractVal" class="form-control"  value="{{$nilai_kontrak}}" readonly>   
                </div>
            </div>
        </div>
    </div>
</div>

@php
    if(!$plan){
@endphp
        <div class="alert alert-danger"><i class="fa fa-remove"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Planning is not yet created</b></div>
@php
    }else{
@endphp
        <div class="panel panel-warning">
            <div class="panel-heading">
                    <b>Detail Planning</b>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4">Planning ID</label>
                        <div class="col-md-8">
                            <input type="text" name="planID" id="planID"  class="form-control" value="{{$plan->kd_rab}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label class="col-md-4">Planning Date</label>
                        <div class="col-md-8">
                            <input type="text" name="planDate" id="planDate" class="form-control"  value="{{$plan->tgl_rab}}" readonly>   
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <b>Account Data</b>
            </div>
            <div class="panel-body">              
                <div class="col-sm-12">
                <table id="accountPlanExpenseTable-V" class="table datatable table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Account</th>
                            <th>Account Type ID</th>
                            <th>Account Type</th>
                            <th style="text-align:right">Account Value</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="success">
                            <th colspan="4"  style="text-align:right">Total :</th>
                            <th style="text-align:right"></th>
                        </tr>
                    </tfoot>
                </table>	
            </div>
            </div>
        </div>
@php } @endphp

@endsection
