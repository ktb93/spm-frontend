@extends ('layouts.dashboard')
@section('page_heading','Account List')

@section('section')

<div class="form-group">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAccount-A" onClick="getAccTypeList('{{config("aplikasi.apiUrl")}}/accTypeList')"><span class="fa  fa-plus"></span>  Add Account</button> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="accountTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Account ID</th>
					<th>Account Name</th>
					<th>Account Type ID</th>
					<th>Account Type</th>
					<th width="75" style="text-align:center">Action</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Add Account -->
<div class="modal" id="modalAccount-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Account</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formAccountAdd" id="formAccountAdd">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account ID</label>
						<div class="col-md-7">
							<input type="text" name="accID"  class="form-control" value="{{$data->accountID}}" placeholder="Account ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Name</label>
						<div class="col-md-7">
							<input type="text" name="accName" style="text-transform: capitalize;" class="form-control" value="" placeholder="Account Name" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type</label>
						<div class="col-md-7">
							<select class="form-control" name="accType" id="accTypeA" required>
                            </select>  
						</div>
					</div>
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeAccount('{{config("aplikasi.apiUrl")}}/storeAccount','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Account -->

<!-- MODALS Edit Account -->
<div class="modal" id="modalAccount-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Account</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formAccountEdit" id="formAccountEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account ID</label>
						<div class="col-md-7">
							<input type="text" name="accID" id="accID" class="form-control" value="" placeholder="Account ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Name</label>
						<div class="col-md-7">
							<input type="text" name="accName" id="accName" style="text-transform: capitalize;" class="form-control" value="" placeholder="Account Name" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type</label>
						<div class="col-md-7">
							<select class="form-control" name="accType" id="accTypeE" required>
                            </select>  
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeAccount('{{config("aplikasi.apiUrl")}}/updateAccount','{{url("gif/loading.gif")}}','Edit')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Account -->
@endsection