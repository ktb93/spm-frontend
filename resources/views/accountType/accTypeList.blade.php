@extends ('layouts.dashboard')
@section('page_heading','Account Type List')

@section('section')

<div class="form-group">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAccType-A"><span class="fa  fa-plus"></span>  Add Account Type</button> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="AccTypeTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Account Type ID</th>
					<th>Account Type</th>
					<th width="75" style="text-align:center">Action</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Add Account Type-->
<div class="modal" id="modalAccType-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Account Type</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formAccTypeAdd" id="formAccTypeAdd">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type ID</label>
						<div class="col-md-7">
							<input type="text" name="accTypeID"  class="form-control" value="{{$data->accTypeID}}" placeholder="Account Type ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type</label>
						<div class="col-md-7">
							<input type="text" name="accType" style="text-transform: capitalize;" class="form-control" value="" placeholder="Account Type" required>   
						</div>
					</div>  
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeAccType('{{config("aplikasi.apiUrl")}}/storeAccType','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Account Type-->

<!-- MODALS Edit Account Type-->
<div class="modal" id="modalAccType-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Account Type</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formAccTypeEdit" id="formAccTypeEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type ID</label>
						<div class="col-md-7">
							<input type="text" name="accTypeID" id="accTypeID" class="form-control" value="" placeholder="Account Type ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type</label>
						<div class="col-md-7">
							<input type="text" name="accType" id="accType" style="text-transform: capitalize;" class="form-control" value="" placeholder="Account Type" required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeAccType('{{config("aplikasi.apiUrl")}}/updateAccType','{{url("gif/loading.gif")}}','Edit')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Account Type -->
@endsection