@extends ('layouts.dashboard')
@section('page_heading','Actual Expense')

@section('section')
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 80%;
   max-width:1400px;
  }
}
</style>

<script type="text/javascript">
    var urlExpense = {!! json_encode(url('/createExpense')) !!};
    var urlViewExpense = {!! json_encode(url('/viewExpense')) !!};
</script>

<a class="btn btn-primary" data-toggle="modal" onclick="getProjectExp('{{config("aplikasi.apiUrl")}}/getProjectExp','{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getContract')" data-target="#modalProjectList-E"><span class="fa  fa-external-link"></span>  Choose Project</a>   

<div class="panel panel-default">
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Project ID</label>
                <div class="col-md-8">
                    <input type="text" name="projectID" id="projectID"  class="form-control" value="" placeholder="Project ID" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Project Description</label>
                <div class="col-md-8">
                    <input type="text" name="project" id="project" style="text-transform: capitalize;" class="form-control" value="" placeholder="Project Description" readonly required>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Location</label>
                <div class="col-md-8">
                    <input type="text" name="location" id="location" style="text-transform: capitalize;" class="form-control" value="" placeholder="Location" readonly required>   
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Plan Start Date</label>
                <div class="col-md-8">
                    <input type="text" name="planStart" id="planStartExp" style="text-transform: capitalize;" class="form-control"  value="" placeholder="Please Select Start Date..." readonly="readonly" required>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Plan Finish Date</label>
                <div class="col-md-8">
                    <input type="text" name="planFinish" id="planFinishExp" style="text-transform: capitalize;" class="form-control" value="" placeholder="Please Select Finish Date..." readonly="readonly" required>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">PIC</label>
                <div class="col-md-8">
                    <input type="text" name="picName" id="picName"  class="form-control" value="" placeholder="Person In Charge" readonly>   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Contract Data</b>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <table id="contractExpenseTable" class="table datatable table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Contract ID</th>
                        <th>Customer ID</th>
                        <th>Contract Number</th>
                        <th>Contract Desc</th>
                        <th>Customer</th>
                        <th>Deadline</th>
                        <th style="text-align:right">Contract Value</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr class="success">
                        <th colspan="6"  style="text-align:right">Total :</th>
                        <th style="text-align:right"></th>
                    </tr>
                </tfoot>
            </table>	
        </div>
    </div>
</div>

<!-- MODALS Project List -->
<div class="modal" id="modalProjectList-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Project List</h4>                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <select  id="type" class="form-control"  name="type" >
                            <option value="plan_start" >Plan Start</option>
                            <option value="plan_finish" >Plan Finish</option>
                        </select>  
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control mx-auto"  placeholder="Start Date" value="" name="filterStart" id="filterStart" onchange="endDateProjectExp('{{config("aplikasi.apiUrl")}}/getProjectExp','{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getContract')" readonly>
                    </div>
                    <div class="col-md-3" id="inputDate"></div>
                    <div class="col-md-1" id="refreshDate"></div>
                </div>
                <hr>

                <table id="projectListTable-E" class="table datatable table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Project ID</th>
                            <th>Project Description</th>
                            <th>Location</th>
                            <th>Plan Start</th>
                            <th>Plan Finish</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>PIC</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                </table>	
            </div>
        </div>
    </div>
</div>
<!-- MODALS Project List -->
@endsection