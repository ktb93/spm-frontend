@extends ('layouts.dashboard')
@section('page_heading','Actual Expense')

@section('section')
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 80%;
   max-width:1400px;
  }
}
</style>

<script type="text/javascript">
    var urlDtlAccount = {!! json_encode(url('/detailAccountExp')) !!};
</script>

<div class="panel panel-default"> 
    <div class="panel-body">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" onclick="backExpense('{{url("expense")}}','{{$data->kd_proyek}}')"><span class="fa fa-arrow-left"></span>  Back</a>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <b>Detail Contract</b>
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Project ID</label>
                <div class="col-md-8">
                    <input type="text" name="projectID" id="projectID"  class="form-control" value="{{$data->kd_proyek}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Project Description</label>
                <div class="col-md-8">
                    <input type="text" name="project" id="project"  class="form-control" value="{{$data->nm_proyek}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract No</label>
                <div class="col-md-8">
                    <input type="hidden" name="expenseID" id="expenseID" value="{{$expenseID->expenseID}}">
                    <input type="hidden" name="newExpenseID" id="newExpenseID">
                    <input type="hidden" name="contractID" id="contractID" value="{{$data->kd_kontrak}}">
                    <input type="text" name="contractNo" id="contractNo"  class="form-control" value="{{$data->no_kontrak}}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract Description</label>
                <div class="col-md-8">
                    <input type="text" name="contract" id="contract" class="form-control" value="{{$data->desk_kontrak}}" readonly>   
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Customer</label>
                <div class="col-md-8">
                    <input type="text" name="customer" id="customer" class="form-control" value="{{$data->nm_customer}}" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Deadline</label>
                <div class="col-md-8">
                    <input type="text" name="deadline" class="form-control"  value="{{$data->deadline}}" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Contract Value</label>
                <div class="col-md-8">
                    <input type="text" name="contractVal" id="contractVal" class="form-control"  value="{{$nilai_kontrak}}" readonly>   
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-primary" id="viewAccountPlan">
    <div class="panel-heading">
        <b>Account Data</b>
    </div>
    <div class="panel-body">
        <a class="btn btn-success" id="btnExpense" onclick="storeExpense('{{config("aplikasi.apiUrl")}}/storeExpense','{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->id_user}}')"><span class="fa fa-suitcase"></span>  Create Expense</a>

        <div class="col-sm-12">
        <table id="accountPlanExpenseTable-V" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
                    <th>Account ID</th>
                    <th>Account</th>
                    <th>Account Type ID</th>
                    <th>Account Type</th>
                    <th style="text-align:right">Planning Value</th>
				</tr>
            </thead>
            <tfoot>
                <tr class="success">
                    <th colspan="4"  style="text-align:right">Total :</th>
                    <th style="text-align:right"></th>
                </tr>
            </tfoot>
		</table>	
	</div>
    </div>
</div>

<div id="detailAccount"></div>

<!-- MODALS Account List -->
<div class="modal" id="modalAccountList" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Account List</h4>                
            </div>
            <div class="modal-body">
                <table id="accountListTable" class="table datatable table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Account ID</th>
                            <th>Account</th>
                            <th>Account Type ID</th>
                            <th>Account Type</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="success">
                            <th colspan="4"  style="text-align:right"></th>
                        </tr>
                    </tfoot>
                </table>
                <div id="createAccount"></div>
            </div>
        </div>
    </div>
</div>
<!-- MODALS Account List -->

<!-- MODALS Add Account -->
<div class="modal" id="modalAccount-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Account</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formAccount-A" id="formAccount-A">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account ID</label>
						<div class="col-md-7">
							<input type="text" name="accID" id="accID-A" class="form-control" placeholder="Account ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Name</label>
						<div class="col-md-7">
							<input type="text" name="accName" style="text-transform: capitalize;" class="form-control" value="" placeholder="Account Name" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Account Type</label>
						<div class="col-md-7">
							<select class="form-control" name="accType" id="accTypeA" required>
                            </select>  
						</div>
					</div>
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeAccPlan('{{config("aplikasi.apiUrl")}}/storeAccount','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Account -->
@endsection