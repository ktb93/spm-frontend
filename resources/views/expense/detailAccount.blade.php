<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Account Data</b>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
        <table id="accountExpenseTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
                    <th>Account ID</th>
                    <th>Account</th>
                    <th>Account Type ID</th>
                    <th>Account Type</th>
                    <th style="text-align:right">Plan Value</th>
                    <th style="text-align:right">Expense Value</th>
                    <th style="text-align:right">Balance</th>
				</tr>
            </thead>
            <tfoot>
                <tr class="success">
                    <th colspan="4"  style="text-align:right">Total :</th>
                    <th style="text-align:right"></th>
                    <th style="text-align:right"></th>
                    <th style="text-align:right"></th>
                </tr>
            </tfoot>
		</table>	
	</div>
    </div>
</div>

<script>
    var contractID = lasturl;
    var urlGetAccountExpense = '{{config("aplikasi.apiUrl")}}getDetailExpense';
    var urlStoreBiaya = '{{config("aplikasi.apiUrl")}}storeDetailExpense';
    $('#accountExpenseTable').DataTable({
        "ordering": false,
        "bInfo" : false,
        "bPaginate": false,
        "processing":true,
        "serverSide":true,
        "bDestroy": true,
        "ajax":{
            "url": urlGetAccountExpense,
            "type":"POST",
            "data":{
                'contractID' : contractID
            },
            "dataSrc": "data",
            "headers": {   
                "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
            },         
        },
        "columnDefs": [ 
            { "targets"  : [ 0, 2 ], "visible": false, }
        ],
        "columns": [   
            { "data": "kd_akun" },
            { "data": "nm_akun" },
            { "data": "kd_tipe_akun" },
            { "data": "tipe_akun" },
            { 
                "data": "jml_rab",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.jml_rab).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
            { "data": "jml_biaya",
                "searchable": false,
                "sortable": false,
                "render": function (id, type, full, meta) { 
                    if(id==null || id=="null"){
                        biaya="";
                    }else{
                        biaya=id;
                    }

                    return  '<div class="text-right" id="message'+full.kd_akun+'" ><a class="btn btn-primary fa fa-pencil btn-xs" onclick="inputBiaya(\''+full.kd_akun+'\',\''+contractID+'\',\''+full.jml_biaya+'\')"   ></a> '+(full.jml_biaya).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+' </div><div id="biaya'+full.kd_akun+'" ></div>';
                } 
            },
            { 
                "data": "balance",
                "render": function (id, type, full, meta) {
                return '<div class="text-right">'+(full.balance).toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")+'</div>';
                }    
            },
        ],
        "rowCallback": function( row, data, index ) {
            if ( data['balance'] < 0 )
            {
                $('td', row).css('background-color', 'pink');
            }
        },  
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
    
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            plan = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 4 ).footer() ).html(
               plan.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );

            expense = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 5 ).footer() ).html(
               expense.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );

            balance = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 6 ).footer() ).html(
               balance.toFixed().replace(/\d(?=(\d{3})+(?!\d))/g, "$&,")
            );
        }  
    });

    function inputBiaya(id,contractID,biaya){
        $('#message'+id).hide('1000');
        select="<form name='formExpense' id='formExpense'>"
        select=select+"<div class='input-group'> "
        select=select+"<input  id='inputEx' class='form-control' name='input"+id+"' value='"+biaya+"' required>";
        select=select+"<span class='input-group-btn'>";
        select=select+'<a class="btn btn-primary" onclick="updateBiaya(\''+id+'\',\''+contractID+'\');"  ><span class="fa fa-check"></span></a>';
        select=select+"</span>";
        select=select+"</div>";
        select=select+"</form>";
        $('#biaya'+id).html(select);
        var $input = $("#inputEx");
            $input.on( "keyup", function( event ) { 
                // When user select text in the document, also abort.
                var selection = window.getSelection().toString();
                if ( selection !== '' ) {
                    return;
                }
                // When the arrow keys are pressed, abort.
                if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                    return;
                }
                var $this = $( this );
                // Get the value.
                var input = $this.val();
                var input = input.replace(/[\D\s\._\-]+/g, "");
                        input = input ? parseInt( input, 10 ) : 0;

                        $this.val( function() {
                            return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                        } );
        } );    
    }

    function updateBiaya(id,contractID){
        if($('#formExpense').valid()){
            swal({
            title: "Input Expense",
            text: "Are you sure to input expense?",
            icon : "warning",
            buttons: ["Cancel", "Yes, I am"],
            closeModal : false
            })
            .then((isConfirm) => {
                if (isConfirm)
                { 
                    jQuery.ajax({
                        type:"POST",
                        url: urlStoreBiaya, 
                        data: {
                                "accountID" : id,
                                "contractID" : contractID,
                                "biaya" : $("#inputEx").val(),
                                },
                        headers: {
                            "Authorization": "Basic Yml6bmV0OmJpem5ldA=="
                        } ,                           
                        success: function(response) {
                            if(response.success == true){
                            swal({
                                title: response.message,
                                text: 'Saving Data Expense',
                                icon: 'success'
                            })
                            }else{
                                swal({
                                    title: response.message,
                                    text: 'Saving Data Expense',
                                    icon: 'error'
                                })
                            }
                            $('#accountExpenseTable').DataTable().ajax.reload();
                        }
                    });  
                }else{
                    swal("Cancelled", "Your data has been cancel.", "error");
                }
            });
        }
    }
</script>
