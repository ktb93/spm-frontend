@extends ('layouts.dashboard')
@section('page_heading','Income Data')

@section('section')
<style>
@media (min-width: 768px) {
  .modal-xl {
    width: 80%;
   max-width:1400px;
  }
}
</style>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="input-group">
            <input type="hidden" name="projectID" id="projectID">
            <input type="text" name="selectPro" id="selectPro"  class="form-control" value="" placeholder="Filter Base On Project ..." required readonly>
            <span class="input-group-btn">
                <a class="btn btn-primary" data-toggle="modal" data-target="#modalProjectInc" onclick="getProjectInc('{{config("aplikasi.apiUrl")}}/getProjectList','{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getIncome')"><span class="fa fa-list"></span></a>
            </span>                        
        </div>
    </div>
</div>

<hr>
<div class="row">
	<div class="col-sm-12">
        <table id="contractIncomeTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
                <tr>
                    <th>Contract ID</th>
                    <th>Customer ID</th>
                    <th>Contract Number</th>
                    <th>Contract Desc</th>
                    <th>Customer</th>
                    <th style="text-align:right">Contract Value</th>
                    <th style="text-align:right">Income Value</th>
                    <th style="text-align:right">Balance</th>
                    <th></th>
                </tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Project List -->
<div class="modal" id="modalProjectInc" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Project List</h4>                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <select  id="type" class="form-control"  name="type" onchange="filterProject()"  >
                            <option value="plan_start" >Plan Start</option>
                            <option value="plan_finish" >Plan Finish</option>
                        </select>  
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control mx-auto"  placeholder="Start Date" value="" name="filterStart" id="filterStart" onchange="endDateProject('{{config("aplikasi.apiUrl")}}/getProjectList','{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getContract')" readonly>
                    </div>
                    <div class="col-md-3" id="inputDate"></div>
                    <div class="col-md-1" id="refreshDate"></div>
                </div>
                <hr>

                <table id="projectListTable" class="table datatable table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Project ID</th>
                            <th>Project Description</th>
                            <th>Location</th>
                            <th>Plan Start</th>
                            <th>Plan Finish</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>PIC</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                </table>	
            </div>
        </div>
    </div>
</div>
<!-- MODALS Project List -->
@endsection