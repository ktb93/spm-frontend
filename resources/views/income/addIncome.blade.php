@extends ('layouts.dashboard')
@section('page_heading','New Project')

@section('section')
<form class="form-group"  name="formProjectAdd" id="formProjectAdd">
    {{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="col-md-4">Project ID</label>
                    <div class="col-md-8">
                        <input type="text" name="projectID"  class="form-control" value="{{$data['projectID']->projectID}}" placeholder="Project ID" readonly>   
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4">Project Description</label>
                    <div class="col-md-8">
                        <input type="text" name="project" style="text-transform: capitalize;" class="form-control" value="" placeholder="Project Description" required>   
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4">Location</label>
                    <div class="col-md-8">
                        <input type="text" name="location" style="text-transform: capitalize;" class="form-control" value="" placeholder="Location" required>   
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    <label class="col-md-4">Plan Start Date</label>
                    <div class="col-md-8">
                        <input type="text" name="planStart" id="planStart" style="text-transform: capitalize;" class="form-control" onchange="validationDate()" value="" placeholder="Please Select Start Date..." readonly="readonly" required>   
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4">Plan Finish Date</label>
                    <div class="col-md-8">
                        <input type="text" name="planFinish" id="planFinish" style="text-transform: capitalize;" class="form-control" onchange="validationDate()" value="" placeholder="Please Select Finish Date..." readonly="readonly" required>   
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4">PIC</label>
                    <div class="col-md-8">
                        <input type="hidden" name='pic' id='pic' value='{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->id_user}}'>
                        <input type="text" name="picName"  class="form-control" value='{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->first_name}} {{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->last_name}}' placeholder="Location" readonly>   
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>Contract Data</b>
        </div>
        <div class="panel-body">
            <a class="btn btn-primary btn-xs" data-toggle="modal" onclick="fContractID('{{config("aplikasi.apiUrl")}}/tmpContractID')" data-target="#modalContract-A"><span class="fa  fa-plus"></span>  Add Contract</a>       
            
            <hr>
            <div class="col-sm-12">
                <table id="contractTmpTable" class="table datatable table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Contract ID</th>
                            <th>Customer ID</th>
                            <th>Contract Number</th>
                            <th>Contract Desc</th>
                            <th>Customer</th>
                            <th>Deadline</th>
                            <th style="text-align:right">Contract Value</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr class="success">
                            <th colspan="6"  style="text-align:right">Total :</th>
                            <th style="text-align:right"></th>
                        </tr>
                    </tfoot>
                </table>	
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-5">
            <textarea name="note" class="form-control" placeholder="Additional Note" style="border:solid 1px orange"></textarea>
        </div>
    </div>
</form>

    <div class="btn-group pull-right pull-bottom">
        <a class="btn btn-default" href="{{url('project')}}"><span class="fa fa-arrow-left"></span>  Back</a>
        <button class="btn btn-primary" onclick="storeProject('{{config("aplikasi.apiUrl")}}/storeProject','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
    </div>

<!-- MODALS Add Contract -->
<div class="modal" id="modalContract-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Contract</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formContractTmp" id="formContractTmp">
					{{ csrf_field() }}
					<div class="form-group row">
                        <input type="hidden" name="projectID" id="projectID" value="{{$data['projectID']->projectID}}">
						<label class="col-md-3 col-md-offset-1">Contract ID</label>
						<div class="col-md-7">
							<input type="hidden" name="ID" id="ID" value="{{$data['contractID']->contractID}}">
                            <input type="text" name="contractID" id="contractID" class="form-control" placeholder="Contract ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Number</label>
						<div class="col-md-7">
							<input type="text" name="contractNo" id="contractNo" class="form-control" value="" placeholder="Contract Number" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Description</label>
						<div class="col-md-7">
							<input type="text" name="contract" id="contract" style="text-transform: capitalize;" class="form-control" value="" placeholder="Contract Description" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer</label>
						<div class="col-md-7">
                            <div class="input-group">
                                <input type="hidden" name="custID" id="custID">
                                <input type="text" name="custName" id="custName" class="form-control" value="" placeholder="Customer Name" readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-warning" onclick="getCustList('{{config("aplikasi.apiUrl")}}/getCustomer','Add')"><span class="fa fa-list"></span></a>
                                </span>
                            </div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Value</label>
						<div class="col-md-7">
							<input type="text"  name="contractVal" id="contractVal" class="form-control" value="" placeholder="Contract Value" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Deadline</label>
						<div class="col-md-7">
							<input type="text"  name="deadline" id="deadline" onchange="validationDeadline()" class="form-control" value="" placeholder="Select Date Deadline..." readonly required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<a class="btn btn-primary" onclick="TmpContract('{{config("aplikasi.apiUrl")}}/tmpContract')"><span class="fa fa-save"></span>  Save</a>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Contract -->

<!-- MODALS Customer List -->
<div class="modal" id="modalCustomerList" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModal('Add')"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Customer List</h4>                
            </div>
            <div class="modal-body">
                    <table id="custTable" class="table datatable table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Customer Name</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                    </table>	
            </div>
        </div>
    </div>
</div>
<!-- MODALS Customer List -->
@endsection