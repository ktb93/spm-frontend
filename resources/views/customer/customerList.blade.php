@extends ('layouts.dashboard')
@section('page_heading','Customer List')

@section('section')

<div class="form-group">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCustomer-A"><span class="fa  fa-plus"></span>  Add Customer</button> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="customerTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Customer ID</th>
					<th>Customer Name</th>
					<th>Address</th>
					<th>Phone</th>
					<th>PIC</th>
					<th>Email</th>
					<th width="75" style="text-align:center">Action</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Add Customer -->
<div class="modal" id="modalCustomer-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Customer</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formCustomerAdd" id="formCustomerAdd">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer ID</label>
						<div class="col-md-7">
							<input type="text" name="custID"  class="form-control" value="{{$data->customerID}}" placeholder="Customer ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer Name</label>
						<div class="col-md-4">
							<input type="text" name="custName" style="text-transform: capitalize;" class="form-control" value="" placeholder="Customer Name" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Address</label>
						<div class="col-md-7">
							<input type="text" name="address" style="text-transform: capitalize;" class="form-control" value="" placeholder="Address" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Phone</label>
						<div class="col-md-7">
							<input type="text" name="phone" style="text-transform: capitalize;" class="form-control input-medium bfh-phone" data-format="dddd-dddd-dddddd" value="" placeholder="Phone Number" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">PIC</label>
						<div class="col-md-7">
							<input type="text" name="pic" style="text-transform: capitalize;" class="form-control" value="" placeholder="Person In Charge" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Email</label>
						<div class="col-md-7">
							<input type="text" name="email" class="form-control" value="" placeholder="E.g spb@mail.com" required>   
						</div>
					</div>   
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeCustomer('{{config("aplikasi.apiUrl")}}/storeCustomer','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Customer -->

<!-- MODALS Edit Customer -->
<div class="modal" id="modalCustomer-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Customer</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formCustomerEdit" id="formCustomerEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer ID</label>
						<div class="col-md-7">
							<input type="text" name="custID" id="custID" class="form-control" value="" placeholder="Customer ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer Name</label>
						<div class="col-md-7">
							<input type="text" name="custName" id="custName" style="text-transform: capitalize;" class="form-control" value="" placeholder="Customer Name" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Address</label>
						<div class="col-md-7">
							<input type="text" name="address" id="address" style="text-transform: capitalize;" class="form-control" value="" placeholder="Address" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Phone</label>
						<div class="col-md-7">
							<input type="text" name="phone" id="phone" style="text-transform: capitalize;" class="form-control input-medium bfh-phone" data-format="dddd-dddd-dddddd"  value="" placeholder="Phone Number" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">PIC</label>
						<div class="col-md-7">
							<input type="text" name="pic" id="pic" style="text-transform: capitalize;" class="form-control" value="" placeholder="Person In Charge" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Email</label>
						<div class="col-md-7">
							<input type="text" name="email" id="email" class="form-control" value="" placeholder="Email" required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeCustomer('{{config("aplikasi.apiUrl")}}/updateCustomer','{{url("gif/loading.gif")}}','Edit')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Customer -->
@endsection