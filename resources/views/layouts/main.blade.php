<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>Project Monitoring System</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link rel="icon" type="image/png" href="{{url('images/icons/favicon.ico')}}"/>
	<link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
	<!-- vendor css -->
	<link rel="stylesheet" type="text/css" href="{{url('vendor/datatables/DataTables-1.10.18/css/dataTables.bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/newdatepicker/css/datepicker.css')}}">

	<link rel="stylesheet" type="text/css" href="{{url('css/passwordcheck.css')}}" />
	<style type="text/css">
		.error{
		color: red;
		font-size: 11px;
		}
		/* Overriding styles */
		::-webkit-input-placeholder {
		font-size: 14px!important;
		}

		:-moz-placeholder { /* Firefox 18- */
			font-size: 14px!important;
		}
		::-moz-placeholder {  /* Firefox 19+ */
			font-size: 14px!important;
		}
		input {
			font-size: 14px!important;
		}
		select {
			font-size: 14px!important;
		}
	</style>
</head>
<body>
	@yield('body')
	
</body>
	<!-- vendor javascript -->
	<script src="{{ asset('assets/scripts/frontend.js') }}" type="text/javascript"></script>
	<script src="{{url('vendor/datatables/datatables.min.js')}}"></script>
	<script src="{{url('vendor/datatables/DataTables-1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{url('vendor/jquery/jquery.validate.min.js')}}"></script>
	<script src="{{url('vendor/sweetalert/dist/sweetalert.min.js')}}"></script>
	<script src="{{url('vendor/BootstrapFormHelpers/dist/js/bootstrap-formhelpers.js')}}"></script>
	<script src="{{url('vendor/bootstrap/js/bootstrap-strength-meter.js')}}"></script>
	<script src="{{url('vendor/password-score-master/dist/js/password-score.js')}}"></script>
	<script src="{{url('vendor/password-score-master/dist/js/password-score-options.js')}}"></script>
	<script src="{{url('vendor/newdatepicker/js/bootstrap-datepicker.js')}}"></script>



	<!-- created javascript -->
	<script src="{{url('js/customer.js')}}"></script>
	<script src="{{url('js/account.js')}}"></script>
	<script src="{{url('js/accountType.js')}}"></script>
	<script src="{{url('js/position.js')}}"></script>
	<script src="{{url('js/user.js')}}"></script>
	<script src="{{url('js/project.js')}}"></script>
	<script src="{{url('js/contract.js')}}"></script>
	<script src="{{url('js/plan.js')}}"></script>
	<script src="{{url('js/expense.js')}}"></script>
	<script src="{{url('js/income.js')}}"></script>

	<script type="text/javascript">
		//-- Get Last url
		var url = window.location.pathname;
		var lasturl = url.split("/").slice(-1)[0];
		//-- Get Last Second Url
		var secondlasturl = url.split("/").slice(-2)[0];

		if (lasturl == 'customerList'){
			getCustomer('{{config("aplikasi.apiUrl")}}/getCustomer','{{url("gif/loading.gif")}}','{{config("aplikasi.apiUrl")}}/deleteCustomer');
		}else if(lasturl == 'accountList'){
			getAccount('{{config("aplikasi.apiUrl")}}/getAccount','{{url("gif/loading.gif")}}','{{config("aplikasi.apiUrl")}}/deleteAccount','{{config("aplikasi.apiUrl")}}/accTypeList');
		}else if(lasturl == 'positionList'){
			getPosition('{{config("aplikasi.apiUrl")}}/getPosition','{{url("gif/loading.gif")}}','{{config("aplikasi.apiUrl")}}/deletePosition');
		}else if(lasturl == 'userList'){
			getUser('{{config("aplikasi.apiUrl")}}/getUser','{{url("gif/loading.gif")}}','{{config("aplikasi.apiUrl")}}/deleteUser','{{config("aplikasi.apiUrl")}}/positionList');
		}else if(lasturl == 'project' || lasturl == 'newProject' || secondlasturl == 'viewProject'){
			getProject('{{config("aplikasi.apiUrl")}}/getProject', '{{url("viewProject")}}');
			getTmpContract('{{config("aplikasi.apiUrl")}}/getTmpContract','{{config("aplikasi.apiUrl")}}/delTmpContract');
			getContract('{{config("aplikasi.apiUrl")}}/getContract',lasturl,'{{config("aplikasi.apiUrl")}}/detailContract');
		}else if(lasturl == 'accTypeList'){
			getAccType('a{{config("aplikasi.apiUrl")}}/getAccType','{{url("gif/loading.gif")}}','{{config("aplikasi.apiUrl")}}/deleteAccType');
		}else if(secondlasturl == 'planExpense'){
			checkPlan('{{config("aplikasi.apiUrl")}}/checkPlan',lasturl);
		}else if(secondlasturl == 'viewPlan' || secondlasturl == 'createExpense'){
			getViewAccount('{{config("aplikasi.apiUrl")}}/getDetailPlan',lasturl);
			checkExpense('{{config("aplikasi.apiUrl")}}/checkExpense',lasturl);
		}else if(secondlasturl == 'viewExpense'){
			getViewExpense('{{config("aplikasi.apiUrl")}}/getDetailExpense',lasturl);
		}else if(lasturl == 'income'){
			getIncome('{{config("aplikasi.apiUrl")}}/getIncome');
		}

		function getUrlVars() {
			var vars = {};
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
				vars[key] = value;
			});
			return vars;
		}

		var planningProjectID = getUrlVars()["planningProjectID"];
		var expenseProjectID = getUrlVars()["expenseProjectID"];

		if(planningProjectID != undefined){
			pickProject('{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getContract',planningProjectID)
		}else if(expenseProjectID != undefined){
			pickProjectExp('{{config("aplikasi.apiUrl")}}/detailProject','{{config("aplikasi.apiUrl")}}/getContract',expenseProjectID)
		}
	</script>
</html>