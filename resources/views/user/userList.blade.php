@extends ('layouts.dashboard')
@section('page_heading','User List')

@section('section')
<div class="form-group">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalUser-A" onClick="getPositionUser('{{config("aplikasi.apiUrl")}}/positionList')"><span class="fa  fa-plus"></span>  Add User</button> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="userTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>User ID</th>
					<th>Firstname</th>
					<th>Lastname</th>
					<th>Username</th>
					<th>Password</th>
					<th>ID Position</th>
					<th>Position</th>
					<th>Email</th>
					<th width="75" style="text-align:center">Action</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Add User -->
<div class="modal" id="modalUser-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add User</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formUserAdd" id="formUserAdd">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">User ID</label>
						<div class="col-md-7">
							<input type="text" name="userID"  class="form-control" value="{{$data->userID}}" placeholder="User ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Firstname</label>
						<div class="col-md-7">
							<input type="text" name="firstname" style="text-transform: capitalize;" class="form-control" value="" placeholder="Firstname" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Lastname</label>
						<div class="col-md-7">
							<input type="text" name="lastname" style="text-transform: capitalize;" class="form-control" value="" placeholder="Lastname" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Username</label>
						<div class="col-md-7">
							<input type="text" name="username" class="form-control" value="" placeholder="Username" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Password</label>
						<div class="col-md-7">
							<input type="password" name="password" id="password" class="form-control" value="" placeholder="Password" required>
							<div style="font-size:12px">Password Strength:</div>
							<div id="password-strength"></div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Re-Password</label>
						<div class="col-md-7">
							<input type="password" name="repassword" class="form-control" value="" placeholder="Re-type Password" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position</label>
						<div class="col-md-7">
							<select class="form-control" name="position" id="positionA" required>
                            </select>  
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Email</label>
						<div class="col-md-7">
							<input type="text" name="email" class="form-control" value="" placeholder="E.g spb@mail.com" required>   
						</div>
					</div>   
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeUser('{{config("aplikasi.apiUrl")}}/storeUser','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add User -->

<!-- MODALS Edit User -->
<div class="modal" id="modalUser-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit User</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formUserEdit" id="formUserEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">User ID</label>
						<div class="col-md-7">
							<input type="text" name="userID" id="userID" class="form-control" value="" placeholder="User ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Firstname</label>
						<div class="col-md-7">
							<input type="text" name="firstname" id="firstname" style="text-transform: capitalize;" class="form-control" value="" placeholder="Firstname" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Lastname</label>
						<div class="col-md-7">
							<input type="text" name="lastname" id="lastname" style="text-transform: capitalize;" class="form-control" value="" placeholder="Lastname" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Username</label>
						<div class="col-md-7">
							<input type="text" name="username" id="username" class="form-control" value="" placeholder="Username" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position</label>
						<div class="col-md-7">
							<select class="form-control" name="position" id="positionE" required>
                            </select>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Email</label>
						<div class="col-md-7">
							<input type="text" name="email" id="email" class="form-control" value="" placeholder="E.g spb@mail.com" required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storeUser('{{config("aplikasi.apiUrl")}}/updateUser','{{url("gif/loading.gif")}}','Edit')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit User -->
@endsection