@extends ('layouts.dashboard')
@section('page_heading','Position List')

@section('section')

<div class="form-group">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalPosition-A"><span class="fa  fa-plus"></span>  Add Position</button> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="positionTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Position ID</th>
					<th>Position</th>
					<th width="75" style="text-align:center">Action</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>

<!-- MODALS Add Position -->
<div class="modal" id="modalPosition-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Position</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formPositionAdd" id="formPositionAdd">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position ID</label>
						<div class="col-md-7">
							<input type="text" name="posID"  class="form-control" value="{{$data->positionID}}" placeholder="Position ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position</label>
						<div class="col-md-7">
							<input type="text" name="position" style="text-transform: capitalize;" class="form-control" value="" placeholder="Position" required>   
						</div>
					</div>
				</form>                 
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storePosition('{{config("aplikasi.apiUrl")}}/storePosition','{{url("gif/loading.gif")}}','Insert')"><span class="fa fa-save"></span>  Save</button>
                </div>   
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Position -->

<!-- MODALS Edit Position -->
<div class="modal" id="modalPosition-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Position</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formPositionEdit" id="formPositionEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position ID</label>
						<div class="col-md-7">
							<input type="text" name="posID" id="posID" class="form-control" value="" placeholder="Position ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Position</label>
						<div class="col-md-7">
							<input type="text" name="position" id="position" style="text-transform: capitalize;" class="form-control" value="" placeholder="Position" required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="storePosition('{{config("aplikasi.apiUrl")}}/updatePosition','{{url("gif/loading.gif")}}','Edit')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Position -->
@endsection