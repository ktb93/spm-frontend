@extends ('layouts.dashboard')
@section('page_heading','Details Project')

@section('section')
<div class="panel panel-default"> 
    <div class="panel-body">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="{{url('project')}}"><span class="fa fa-arrow-left"></span>  Back</a>
            @php
                if ($data->status == 'Progress'){
            @endphp
            <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalProject-E"><span class="fa fa-edit"></span>  Update</button>
                @php
                        if($data->progress == '100'){
                @endphp
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalProject-C"><span class="fa fa-archive"></span>  Close Project</button>
                @php }else{ @endphp
                    <button class="btn btn-danger btn-sm" onclick="cancelProject('{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->id_user}}','{{config("aplikasi.apiUrl")}}/cancelProject','{{url("gif/loading.gif")}}')"><span class="fa fa-times"></span>  Cancel Project</button>
                @php } @endphp
            @php } @endphp
        </div>
    </div>
</div>
@php
if ($data->status == 'Progress'){
@endphp
    <div class="panel panel-info">
    <div class="panel-heading">
        <b>Status : On Progress ({{$data->progress}}%)</b>
    </div>
@php
}elseif($data->status == 'Cancel'){
@endphp
    <div class="panel panel-danger">
    <div class="panel-heading">
        <b>Status : Cancel</b>
    </div>    
@php
}elseif($data->status == 'Closed'){
@endphp
    <div class="panel panel-success">
    <div class="panel-heading">
        <b>Status : Closed</b>
    </div>
@php } @endphp
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Project ID</label>
                <div class="col-md-8">
                    <input type="text" name="projectID" id="projectID" class="form-control" value="{{$data->kd_proyek}}" placeholder="Project ID" readonly>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Project Description</label>
                <div class="col-md-8">
                    <input type="text" name="project" style="text-transform: capitalize;" class="form-control" value="{{$data->nm_proyek}}" placeholder="Project Description" readonly required>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Location</label>
                <div class="col-md-8">
                    <input type="text" name="location" style="text-transform: capitalize;" class="form-control" value="{{$data->location}}" placeholder="Location" readonly required>   
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row">
                <label class="col-md-4">Plan Start Date</label>
                <div class="col-md-8">
                    <input type="text" name="planStart" id="pStart" class="form-control" value="{{$date_start}}" placeholder="Please Select Start Date..." readonly required>   
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4">Plan Finish Date</label>
                <div class="col-md-8">
                    <input type="text" name="planFinish" id="pFinish" class="form-control" value="{{$date_finish}}" placeholder="Please Select Finish Date..." readonly required>   
                </div>
            </div>
            <div class="form-group row">
                    <label class="col-md-4">PIC</label>
                    <div class="col-md-8">
                        <input type="text" name="picName"  class="form-control" value="{{$data->first_name}} {{$data->last_name}}" placeholder="Location" readonly>   
                    </div>
                </div>
        </div>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">
        <b>Contract Data</b>
    </div>
    <div class="panel-body">
        <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalContract-A"><span class="fa  fa-plus"></span>  Add Contract</a>       
        <hr>
        <div class="col-sm-12">
            <table id="contractTable" class="table datatable table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Contract ID</th>
                        <th>Customer ID</th>
                        <th>Contract Number</th>
                        <th>Contract Desc</th>
                        <th>Customer</th>
                        <th>Deadline</th>
                        <th style="text-align:right">Contract Value</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr class="success">
                        <th colspan="6"  style="text-align:right">Total :</th>
                        <th style="text-align:right"></th>
                    </tr>
                </tfoot>
            </table>	
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-md-1">Note :</label>
    <div class="col-md-5">
        <textarea name="note" class="form-control" placeholder="Additional Note" style="border:solid 1px orange" readonly>{{$data->note}}</textarea>
    </div>
</div>

<!-- MODALS Edit Project -->
<div class="modal" id="modalProject-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Project</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formProjectEdit" id="formProjectEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Project ID</label>
						<div class="col-md-7">
							<input type="text" name="projectID"  class="form-control" value="{{$data->kd_proyek}}" placeholder="Project ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Project Description</label>
						<div class="col-md-7">
							<input type="text" name="project" style="text-transform: capitalize;" class="form-control" value="{{$data->nm_proyek}}" placeholder="Project Description" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Location</label>
						<div class="col-md-7">
							<input type="text" name="location" style="text-transform: capitalize;" class="form-control" value="{{$data->location}}" placeholder="Location" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Plan Start Date</label>
						<div class="col-md-7">
							<input type="text"  name="planStart" id="planStart" onchange="validationDate()" class="form-control" value="{{$date_start}}" placeholder="Please Select Start Date..." readonly required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Plan Finish Date</label>
						<div class="col-md-7">
							<input type="text" name="planFinish" id="planFinish" onchange="validationDate()" class="form-control" value="{{$date_start}}" placeholder="Please Select Finish Date..." readonly required>   
						</div>
                    </div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">PIC</label>
						<div class="col-md-7">
                            <div class="input-group">
                                <input type="hidden" name="userID" id="userID" value="{{$data->id_user}}">
                                <input type="text" name="fullName" id="fullName" class="form-control" value="{{$data->first_name}} {{$data->last_name}}" readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-warning" onclick="userProjectList('{{config("aplikasi.apiUrl")}}/getUser')"><span class="fa fa-list"></span></a>
                                </span>
                            </div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Note</label>
						<div class="col-md-7">
                            <textarea name="note" class="form-control" placeholder="Additional Note" style="border:solid 1px orange">{{$data->note}}</textarea>
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<button class="btn btn-primary" onclick="updateProject('{{config("aplikasi.apiUrl")}}/updateProject','{{url("gif/loading.gif")}}')"><span class="fa fa-save"></span>  Save</button>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Project -->

<!-- MODALS User List -->
<div class="modal" id="modalUserList" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModalUser()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Customer List</h4>                
            </div>
            <div class="modal-body">
                    <table id="userProjectTable" class="table datatable table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>FirsName</th>
                                <th>LastName</th>
                                <th>Full Name</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                    </table>	
            </div>
        </div>
    </div>
</div>
<!-- MODALS User List -->

<!-- MODALS Close Project -->
<div class="modal" id="modalProject-C" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Closing Project</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formContractAdd" id="formProjectC">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Closed By</label>
						<div class="col-md-7">
                            <input type="hidden" name="closedBy" id="closedBy" value='{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->id_user}}'>
							<input type="text" name="closedByName" id="closedByName" class="form-control" value='{{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->first_name}} {{Cache::get(config("aplikasi.codeRedis").Cookie::get("api_token"))->last_name}}' placeholder="Actual Finish Date" readonly>   
						</div>
					</div> 
                    <div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Actual Start Date</label>
						<div class="col-md-7">
							<input type="text" name="actualStart" id="actualStart" onchange="validationClosing()" class="form-control" value="" placeholder="Actual Start Date" readonly required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Actual Finish Date</label>
						<div class="col-md-7">
							<input type="text" name="actualFinish" id="actualFinish" onchange="validationClosing()" class="form-control" value="" placeholder="Actual Finish Date" readonly required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Note</label>
						<div class="col-md-7">
                            <textarea name="note" class="form-control" placeholder="Additional Note" style="border:solid 1px orange">{{$data->note}}</textarea>
						</div>
                    </div>
                    <input type="hidden" name="projectID" id="projectID" value="{{$data->kd_proyek}}">
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<a class="btn btn-primary" onclick="closeProject('{{config("aplikasi.apiUrl")}}/closeProject')"><span class="fa fa-save"></span>  Save</a>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Close Project -->

<!-- MODALS Add Contract -->
<div class="modal" id="modalContract-A" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Add Contract</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formContractAdd" id="formContractAdd">
					{{ csrf_field() }}
					<div class="form-group row">
                    <input type="hidden" name="projectID" id="projectID" value="{{$data->kd_proyek}}"> 
						<label class="col-md-3 col-md-offset-1">Contract ID</label>
						<div class="col-md-7">
                            <input type="text" name="contractID" id="contractID" value="{{$contractID->contractID}}" class="form-control" placeholder="Contract ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Number</label>
						<div class="col-md-7">
							<input type="text" name="contractNo" id="contractNo" class="form-control" value="" placeholder="Contract Number" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Description</label>
						<div class="col-md-7">
							<input type="text" name="contract" id="contract" style="text-transform: capitalize;" class="form-control" value="" placeholder="Contract Description" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer</label>
						<div class="col-md-7">
                            <div class="input-group">
                                <input type="hidden" name="custID" id="custID">
                                <input type="text" name="custName" id="custName" class="form-control" value="" placeholder="Customer Name" readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-warning" onclick="getCustList('{{config("aplikasi.apiUrl")}}/getCustomer','Add')"><span class="fa fa-list"></span></a>
                                </span>
                            </div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Value</label>
						<div class="col-md-7">
							<input type="text"  name="contractVal" id="contractVal" class="form-control" value="" placeholder="Contract Value" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Deadline</label>
						<div class="col-md-7">
							<input type="text"  name="deadline" id="deadline" onchange="validDeadline()" class="form-control" value="" placeholder="Select Date Deadline..." readonly required>   
						</div>
					</div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<a class="btn btn-primary" onclick="storeContract('{{config("aplikasi.apiUrl")}}/storeContract')"><span class="fa fa-save"></span>  Save</a>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Add Contract -->

<!-- MODALS Customer List -->
<div class="modal" id="modalCustomerList" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModal('Edit')"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Customer List</h4>                
            </div>
            <div class="modal-body">
                    <table id="custTable" class="table datatable table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Customer ID</th>
                                <th>Customer Name</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                    </table>	
            </div>
        </div>
    </div>
</div>
<!-- MODALS Customer List -->

<!-- MODALS Edit Contract -->
<div class="modal" id="modalContract-E" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Edit Contract</h4>                
            </div>
            <div class="modal-body">
				<form class="form-group"  name="formContractEdit" id="formContractEdit">
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract ID</label>
						<div class="col-md-7">
                            <input type="text" name="contractID-E" id="contractID-E" class="form-control" placeholder="Contract ID" readonly>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Number</label>
						<div class="col-md-7">
							<input type="text" name="contractNo-E" id="contractNo-E" class="form-control" value="" placeholder="Contract Number" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Description</label>
						<div class="col-md-7">
							<input type="text" name="contract-E" id="contract-E" style="text-transform: capitalize;" class="form-control" value="" placeholder="Contract Description" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Customer</label>
						<div class="col-md-7">
                            <div class="input-group">
                                <input type="hidden" name="custID-E" id="custID-E">
                                <input type="text" name="custName-E" id="custName-E" class="form-control" value="" placeholder="Customer Name" readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-warning" onclick="getCustList('{{config("aplikasi.apiUrl")}}/getCustomer','Edit')"><span class="fa fa-list"></span></a>
                                </span>
                            </div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Contract Value</label>
						<div class="col-md-7">
							<input type="text"  name="contractVal-E" id="contractVal-E" class="form-control" value="" placeholder="Contract Value" required>   
						</div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 col-md-offset-1">Deadline</label>
						<div class="col-md-7">
							<input type="text"  name="deadline-E" id="deadline-E" onchange="validDeadline()" class="form-control" value="" placeholder="Select Date Deadline..." readonly required>   
						</div>
                    </div>
				</form>                    
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<a class="btn btn-primary" onclick="updateContract('{{config("aplikasi.apiUrl")}}/updateContract')"><span class="fa fa-save"></span>  Save</a>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Edit Contract -->

<!-- MODALS Close Contract -->
<div class="modal" id="modalContract-S" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="defModalHead">Status Contract</h4>                
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <input type="hidden" name="contractID-S" id="contractID-S">
                    <label class="col-md-3 col-md-offset-1">Status</label>
                    <div class="col-md-7">
                        <select class="form-control" name="status" id="status" required>
                            <option value="Progress">Progress</option>
                            <option value="Finish">Finish</option>
                            <option value="Cancel">Cancel</option>
                        </select>  
                    </div>
                </div>       
			</div>
            <div class="modal-footer">
				<div class="btn-group pull-right">
					<a class="btn btn-primary" onclick="closeContract('{{config("aplikasi.apiUrl")}}/closeContract')"><span class="fa fa-save"></span>  Save</a>
                </div>     
            </div>
        </div>
    </div>
</div>
<!-- MODALS Close Contract -->
@endsection