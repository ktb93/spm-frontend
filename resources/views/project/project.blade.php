@extends ('layouts.dashboard')
@section('page_heading','Project')

@section('section')
<div class="panel panel-default"> 
    <div class="panel-body">
		<div class="col-md-2">
			<select  id="type" class="form-control"  name="type" onchange="filterProject()"  >
				<option value="plan_start" >Plan Start</option>
				<option value="plan_finish" >Plan Finish</option>
			</select>  
		</div>
		<!-- <div id="valueInput"> -->
		<div class="col-md-2">
			<input type="text" class="form-control mx-auto"  placeholder="Start Date" value="" name="filterStart" id="filterStart" onchange="endDateInput('{{config("aplikasi.apiUrl")}}/getProject', '{{url("viewProject")}}','refresh')" readonly>
		</div>
		<div class="col-md-2" id="inputDate"></div>
		<div class="col-md-1" id="refreshDate"></div>
		<!-- </div> -->
	</div>
</div>

<div class="form-group">
	<a class="btn btn-primary" href="{{url('newProject')}}"><span class="fa  fa-building"></span>  Create Project</a> 
</div>
<div class="row">
	<div class="col-sm-12">
        <table id="projectTable" class="table datatable table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Project ID</th>
                    <th>Project Description</th>
                    <th>Location</th>
                    <th>Plan Start</th>
                    <th>Plan Finish</th>
                    <th>Progress</th>
					<th width="75" style="text-align:center">Action</th>
					<th>Status</th>
				</tr>
			</thead>
		</table>	
	</div>
</div>
@endsection