<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{url('images/key.png')}}"/>
	<link rel="stylesheet" type="text/css" href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('fonts/iconic/css/material-design-iconic-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/animate/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/css-hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/animsition/css/animsition.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/select2/select2.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('vendor/daterangepicker/daterangepicker.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('css/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('css/custom.css')}}">

</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="col-sm-5 float-right">
				<div class="wrap-login100">
					<form class="login100-form validate-form" id="login" action="#" method="post">
						{{ csrf_field() }}
						<span class="login100-form-logo">
							<i><img src="{{url('images/logo_small.png')}}" class="img-responsive img-rounded" alt="Logo"></i>
						</span>
						<span class="login100-form-title p-b-20 p-t-15">
							Log in
						</span>
						<div class="wrap-input100 validate-input" data-validate = "Enter username">
							<input class="input100" type="text" name="username" id="username" placeholder="Username">
							<span class="focus-input100" data-placeholder="&#xf207;"></span>
						</div>

						<div class="wrap-input100 validate-input" data-validate="Enter password">
							<input class="input100" type="password" name="password" id="password" placeholder="Password">
							<span class="focus-input100" data-placeholder="&#xf191;"></span>
						</div>
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>
						<div class="container-login100-form-btn">
							<button class="login100-form-btn" id="submitLogin">
								Login
							</button>
						</div>
						<div class="text-center p-t-5">
							<a class="txt1" href="#">
								Forgot Password?
							</a>
						</div>
					</form>			
				</div>
			</div>
		</div>
	</div>

	<div id="dropDownSelect1"></div>

	<script src="{{url('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{url('vendor/animsition/js/animsition.min.js')}}"></script>
	<script src="{{url('vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{url('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{url('vendor/select2/select2.min.js')}}"></script>
	<script src="{{url('vendor/daterangepicker/moment.min.js')}}"></script>
	<script src="{{url('vendor/daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{url('vendor/countdowntime/countdowntime.js')}}"></script>
	<script src="{{url('vendor/sweetalert/dist/sweetalert.min.js')}}"></script>
	<script src="{{url('js/main.js')}}"></script>

	<script>
		$('#submitLogin').click(function(e){
			e.preventDefault();
			var form = $('#login');
			var username = $("#username").val();
			var password = $("#password").val();

			if (username == null || username == "")
			{
				setTimeout(function () {swal("Warning!", "Your username is empty!");}, 1000);
			}
			else if (password == null || password == "")
			{
				setTimeout(function () {swal("Warning!", "Your password is empty!");}, 1000);
			}
			else
			{
				jQuery.ajax({
					type:"POST",
					url: "{{ url('loginApp') }}",
					data: form.serialize(),
					beforeSend: function(){
						swal({
							title: "Loading...",
							text: "Please wait!",
							icon: '{{url("gif/loading.gif")}}',      
							button: false
						});
					},
					success: function(data) {
						if(data.success == true)
						{
							window.location.href = "{{ url('/dashboard') }}"; 
						}
						else
						{
							swal("Error", data.msg, "error");
						}
					}
				});	
			}
		});
	</script>  

</body>
</html>