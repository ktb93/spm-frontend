<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::redirect('/', '/login',301);
Route::post('loginApp', 'Login\LoginController@loginApp');
Route::get('logout', 'Login\LoginController@logoutApp');
Route::get('menu', 'Menu\MenuController@menuList');


Route::group(['middleware'=>'checklogin'],function ()
{
    Route::get('/', function () {
        return redirect('login');
    });
    Route::get('login', 'Login\LoginController@indexLogin');

    //Menu Template
    Route::get('dashboard', 'TemplateController@dashboard');
    Route::get('charts', 'TemplateController@charts');
    Route::get('tables', 'TemplateController@tables');
    Route::get('forms', 'TemplateController@forms');
    Route::get('progress', 'TemplateController@progress');
    Route::get('panels', 'TemplateController@panels');
    Route::get('buttons', 'TemplateController@buttons');
    Route::get('notifications', 'TemplateController@notifications');
    Route::get('typography', 'TemplateController@typography');
    Route::get('icons', 'TemplateController@icons');
    Route::get('grid', 'TemplateController@grid');
    Route::get('blank', 'TemplateController@blanks');
    Route::get('loginTemplate', 'TemplateController@login');
    Route::get('documentation', 'TemplateController@documentation');

    //Menu Application
    Route::get('customerList', 'Customer\CustomerController@customerList');
    Route::get('accTypeList', 'AccountType\accTypeController@accTypeList');
    Route::get('accountList', 'Account\AccountController@accountList');
    Route::get('positionList', 'Position\PositionController@positionList');
    Route::get('userList', 'User\UserController@userList');
    Route::get('project', 'Project\ProjectController@projectData');
    Route::get('newProject', 'Project\ProjectController@newProject');
    Route::get('viewProject/{kd_proyek}', 'Project\ProjectController@viewProject');
    Route::get('planning', 'Planning\PlanController@planExpense');
    Route::get('planExpense/{kd_kontrak}', 'Planning\PlanController@detailPlanExpense');
    Route::get('detailAccount', 'Planning\PlanController@detailAccount');
    Route::get('viewPlan/{kd_kontrak}', 'Planning\PlanController@viewPlanExpense');
    Route::get('expense', 'Expense\ExpenseController@expense');
    Route::get('createExpense/{kd_kontrak}', 'Expense\ExpenseController@detailExpense');
    Route::get('detailAccountExp', 'Expense\ExpenseController@detailAccount');
    Route::get('viewExpense/{kd_kontrak}', 'Expense\ExpenseController@viewExpense');
    Route::get('income', 'Income\IncomeController@incomeData');
});


